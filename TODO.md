Pages 
  + replace all the SSP lists to more dynamic ones with search
  + create admin folder for js controllers
  + List Users and search by name and email
  + List cookies (remove this page)
    + by date
  + For each user show :
     + list paginated cookies by date and choose to list:
        + list choosen products
        + list ingrediens
        + list products by choosen recipes
  + Add, Edit Image for Ingredient, Recipe and Product with Id for name
  + Ingredients List, Add, Edit, Delete
  + Tag   List, Add, Edit, Delete
  + Recipe List, Add, Edit, Delete
    + Add , Edit Products
  + Product List, Add, Edit, Delete
    + Add, Edit with product type
  + Page for statistics 
   + Send a CSV from the total number of choosen recipes and products by day

+ Nav menu
  + Admins will have CRUD on Recipes, Ingredients, Tags and Products
    For adding Ingredients , Tags will use autocomplete and an add button.

+ Search Recipes
  + Form:
    + By name or
    + By ingredients or
    + By Tags

  + List Recipes
    + Each recipe has a check box
    + Title
    + description
    + pages
    + is checked

  + if just one recipe is checked, then it will show a button to show the cookie's choices


+ List of ingredients and choosen recipes
  + list choosen recipes
  + list ingredients
  + show products for each ingredient and choose
  + show products from each recipe and choose
  + list of choosen products
  + Button to create a pdf
  + Finish button to recreate the cookie
    + If they are not users, tell them to register or login
  - Until decide on working with a market
    dont use this button <button data-bind="click: function(){showDialog(true)}">Τελείωσα</button>


+ Page for the recipes and products that are SSP for SEO

+ Page to change user's password

+ Front page show latest recipes and products

+ Add choose button to index and single items

+ Seperate in a template the baskets of choosen recipes and products.
  They will be in any list or single pages of recipes and products

+ Add text editor for recipes and products
  + Make sure that the templates will take the html of the text editors

- Change architect of the web site
  - Use angular rather than knockout, but dont use the router of angular. 
    However change the templates {{}} of server side
    beego.TemplateLeft = "<<<"
    beego.TemplateRight = ">>>"

- Model articles [Title,LinkName, Text, UserId, Date]
  - CRUD over /articles/:LinkName if LinkName is not added, use the ObjectID

- Admin list of users have a button to make admin
  - Show for each user, if shop owner, admin or nothing
- Admin tag list to Single page

- For users
  - add pictures
  - comments to recipes

- Mobile App
  - List cookies by date
  - Each cookie, show list of ingredients
  - If user is 1 km closer to a shop, show only the products from that super market.
  


- Model Shop [Name, description, UserId]
  - Will have a list of locations by shop {Address, Phone, Email, Location}
  - Will have a list of products

- User from settings enables shop owner
- If shop owner enabled, create a new navigation
  - Edit Shop
    - Edit Name, Description
  - List Locations [Address, Geolocation, Phone]
  - List his Products  
    - Add, Edit, Delete Product
  - Statistics
  - List deliveries

- Rather than τελειωσε , use "save" or "cancel" 

- Button to buy choosen products if shop supports delivery
