package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
)

type ListCookiesCtrl struct {
	beego.Controller
}

func (c *ListCookiesCtrl) GetTemplateCookies() {
	c.TplNames = "admin/cookies.tpl"
}

func (c *ListCookiesCtrl) SearchCookies() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	is_finished, err := c.GetBool("IsFinished")
	if err != nil {
		log.Println(err.Error())
	}

	is_user, err := c.GetBool("IsUser")
	if err != nil {
		log.Println(err.Error())
	}
	cookies, err := models.SearchCookies(db, name,
		is_user, is_finished, size, page)
	if err != nil {
		log.Println(err.Error())
	}
	c.Data["json"] = cookies
	c.ServeJson()
}
