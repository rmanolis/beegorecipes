package admin

import (
	"github.com/astaxie/beego"
)

type ListIngredientsCtrl struct {
	beego.Controller
}

func (c *ListIngredientsCtrl) Get() {

	c.TplNames = "admin/ingredients.tpl"
}
