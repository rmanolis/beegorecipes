package admin

import (
	"github.com/astaxie/beego"
)

type ListProductsCtrl struct {
	beego.Controller
}

func (c *ListProductsCtrl) Get() {

	c.TplNames = "admin/products.tpl"
}
