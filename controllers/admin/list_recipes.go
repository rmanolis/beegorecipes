package admin

import (
	"github.com/astaxie/beego"
)

type ListRecipesCtrl struct {
	beego.Controller
}

func (c *ListRecipesCtrl) Get() {

	c.TplNames = "admin/recipes.tpl"
}
