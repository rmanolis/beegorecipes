package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
)

type ListTagsCtrl struct {
	beego.Controller
}

func (c *ListTagsCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	beego.ReadFromRequest(&c.Controller)
	lo, err := models.GetTags(db, size, page)

	if err != nil {
		log.Println(err.Error())
	}
	c.Data["paginator"] = utils.NewPaginator(c.Ctx.Request, size, lo.Total)
	c.Data["tags"] = lo.Objects.([]models.Tag)

	c.TplNames = "admin/tags.tpl"
}
