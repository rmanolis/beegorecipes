package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
)

type ListUsersCtrl struct {
	beego.Controller
}

func (c *ListUsersCtrl) GetTemplateUsers() {
	c.TplNames = "admin/users.tpl"
}

func (c *ListUsersCtrl) SearchUsers() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	email := c.GetString("Email")
	users, err := models.SearchUsers(db, name, email, size, page)
	if err != nil {
		log.Println(err.Error())
	}
	c.Data["json"] = users
	c.ServeJson()
}
