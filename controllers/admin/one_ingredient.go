package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"log"
	"net/http"

	"encoding/json"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type OneIngredientCtrl struct {
	beego.Controller
}

func setIngredient(c *OneIngredientCtrl,
	db *mgo.Database, ing *models.Ingredient) {
	if !ing.Id.Valid() {
		ing.Id = bson.NewObjectId()
	}
	ing.Name = c.GetString("Name")
	tags := []string{}
	st := c.GetString("Tags")
	log.Println(st)
	json.Unmarshal([]byte(st), &tags)
	log.Println(tags)
	ing.Tags = models.NamesToTags(db, tags)
	err := c.SaveToFile("file", "static/images/i_"+ing.Id.Hex()+".jpg")
	if err != nil {
		log.Println(err.Error())
	}

}

func (c *OneIngredientCtrl) Get() {
	beego.ReadFromRequest(&c.Controller)
	c.TplNames = "admin/ingredient.tpl"
}

func (c *OneIngredientCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	ing := new(models.Ingredient)
	setIngredient(c, db, ing)
	err := ing.Insert(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}
	c.Ctx.ResponseWriter.WriteHeader(http.StatusAccepted)
	c.ServeJson()
}

func (c *OneIngredientCtrl) Put() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.Output.SetStatus(http.StatusNotAcceptable)
		return
	}
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bson.ObjectIdHex(id), ing)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}

	ing.Name = c.GetString("Name")
	setIngredient(c, db, ing)
	err = ing.Update(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}
	c.Ctx.ResponseWriter.WriteHeader(http.StatusAccepted)

}

func (c *OneIngredientCtrl) Delete() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bson.ObjectIdHex(id), ing)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	ing.Remove(db)
	c.ServeJson()
}
