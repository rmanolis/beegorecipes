package admin

import (
	//"beegorecipes/utils"
	"log"

	"github.com/astaxie/beego"
)

type OnePhotoCtrl struct {
	beego.Controller
}

func (c *OnePhotoCtrl) Post() {
	err := c.SaveToFile("file", "static/images/ha.jpg")
	if err != nil {
		log.Println(err.Error())
	}
	c.ServeJson()

}
