package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"encoding/json"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type OneProductCtrl struct {
	beego.Controller
}

func (c *OneProductCtrl) Get() {
	_, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	beego.ReadFromRequest(&c.Controller)
	c.TplNames = "admin/product.tpl"
}

func setProduct(c *OneProductCtrl, db *mgo.Database, prod *models.Product) {
	if !prod.Id.Valid() {
		prod.Id = bson.NewObjectId()
	}

	prod.Name = c.GetString("Name")
	prod.ProductType = c.GetString("ProductType")
	prod.Description = c.GetString("Description")
	prod.Price, _ = c.GetFloat("Price")
	tags := []string{}
	st := c.GetString("Tags")
	json.Unmarshal([]byte(st), &tags)
	prod.Tags = models.NamesToTags(db, tags)

	ings := []string{}
	si := c.GetString("Ingredients")
	json.Unmarshal([]byte(si), &ings)
	prod.Ingredients = models.NamesToIngredients(db, ings)
	err := c.SaveToFile("file", "static/images/p_"+prod.Id.Hex()+".jpg")
	if err != nil {
		log.Println(err.Error())
	}

}

func (c *OneProductCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	prod := new(models.Product)
	setProduct(c, db, prod)
	err := prod.Insert(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}
	c.Ctx.ResponseWriter.WriteHeader(http.StatusAccepted)
	c.ServeJson()
}

func (c *OneProductCtrl) Put() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.Output.SetStatus(http.StatusNotAcceptable)
		return
	}
	prod := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), prod)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}

	setProduct(c, db, prod)
	log.Println(prod)
	err = prod.Update(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}
	c.Ctx.ResponseWriter.WriteHeader(http.StatusAccepted)

}

func (c *OneProductCtrl) Delete() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	prod := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), prod)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	prod.Remove(db)
	c.ServeJson()
}
