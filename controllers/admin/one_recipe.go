package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"encoding/json"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type OneRecipeCtrl struct {
	beego.Controller
}

func (c *OneRecipeCtrl) Get() {
	_, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	beego.ReadFromRequest(&c.Controller)
	c.TplNames = "admin/recipe.tpl"
}

func setRecipe(c *OneRecipeCtrl,
	db *mgo.Database, rec *models.Recipe) {
	if !rec.Id.Valid() {
		rec.Id = bson.NewObjectId()
	}

	rec.Name = c.GetString("Name")
	rec.Servings = c.GetString("Servings")
	rec.Steps = c.GetString("Steps")
	tags := []string{}
	st := c.GetString("Tags")
	json.Unmarshal([]byte(st), &tags)
	rec.Tags = models.NamesToTags(db, tags)

	ings := []string{}
	si := c.GetString("Ingredients")
	json.Unmarshal([]byte(si), &ings)
	rec.Ingredients = models.NamesToIngredients(db, ings)

	prods := []string{}
	sp := c.GetString("Products")
	json.Unmarshal([]byte(sp), &prods)
	rec.Products = models.NamesToProducts(db, prods)
	err := c.SaveToFile("file", "static/images/r_"+rec.Id.Hex()+".jpg")
	if err != nil {
		log.Println(err.Error())
	}

}

func (c *OneRecipeCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	rec := new(models.Recipe)
	setRecipe(c, db, rec)
	err := rec.Insert(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}
	c.Ctx.ResponseWriter.WriteHeader(http.StatusAccepted)
	c.ServeJson()
}

func (c *OneRecipeCtrl) Put() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.Output.SetStatus(http.StatusNotAcceptable)
		return
	}
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), rec)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}

	setRecipe(c, db, rec)
	log.Println(rec)
	err = rec.Update(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}
	c.Ctx.ResponseWriter.WriteHeader(http.StatusAccepted)

}

func (c *OneRecipeCtrl) Delete() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), rec)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	rec.Remove(db)
	c.ServeJson()
}
