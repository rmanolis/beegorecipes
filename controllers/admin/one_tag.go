package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
	"log"
)

type OneTagCtrl struct {
	beego.Controller
}

func (c *OneTagCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	beego.ReadFromRequest(&c.Controller)
	c.TplNames = "admin/tag.tpl"

	id := c.Ctx.Input.Param(":id")
	tag := new(models.Tag)
	is_new := true
	if id != "add" {
		if bson.IsObjectIdHex(id) {
			err := models.FindTagId(db, bson.ObjectIdHex(id), tag)
			if err != nil {
				c.Redirect("/admin/tags", 302)
				return
			}
			is_new = false
		} else {

			c.Redirect("/admin/tags", 302)
			return
		}
	}
	c.Data["is_new"] = is_new
	c.Data["tag"] = tag

}

func (c *OneTagCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	beego.ReadFromRequest(&c.Controller)

	tag := new(models.Tag)
	tag.Name = c.GetString("Name")
	tag.Insert(db)

	c.Redirect("/admin/tags", 302)
}

func (c *OneTagCtrl) Put() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	beego.ReadFromRequest(&c.Controller)

	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Redirect("/admin/tags", 302)
		return
	}
	tag := new(models.Tag)
	err := models.FindTagId(db, bson.ObjectIdHex(id), tag)
	if err != nil {
		c.Redirect("/admin/tags", 302)
		return
	}
	tag.Name = c.GetString("Name")
	tag.Update(db)
	c.Redirect("/admin/tags", 302)
}

func (c *OneTagCtrl) Delete() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Redirect("/admin/tags", 302)
		return
	}
	tag := new(models.Tag)
	err := models.FindTagId(db, bson.ObjectIdHex(id), tag)
	if err != nil {
		c.Redirect("/admin/tags", 302)
		return
	}
	tag.Remove(db)
	c.Redirect("/admin/tags", 302)
}
