package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"log"
	"net/http"

	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type OneUserCtrl struct {
	beego.Controller
}

func (c *OneUserCtrl) GetPage() {
	c.TplNames = "admin/user.tpl"
}

func (c *OneUserCtrl) GetUserCookies() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	user, err := models.GetUserById(db, bid)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	size, page := utils.SizePage(&c.Controller)
	ls, err := user.GetCookies(db, size, page)
	if err != nil {
		log.Println(err.Error())
	}

	c.Data["json"] = ls
	c.ServeJson()
}

func (c *OneUserCtrl) GetUserIngredients() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie, status := getCookie(db, c)
	if status > 0 {
		c.Ctx.Output.SetStatus(status)
		return
	}
	ings, err := cookie.GetIngredientsByRecipes(db)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println(ings)
	c.Data["json"] = ings
	c.ServeJson()
}

func getCookie(db *mgo.Database, c *OneUserCtrl) (*models.Cookie, int) {
	cookie_id := c.GetString("CookieId")
	if !bson.IsObjectIdHex(cookie_id) {
		return nil, http.StatusNotFound
	}
	cid := bson.ObjectIdHex(cookie_id)
	cookie, err := models.FindCookieId(db, cid)
	if err != nil {
		return nil, http.StatusNotFound

	}
	return cookie, 0
}

func (c *OneUserCtrl) GetUserProducts() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie, status := getCookie(db, c)
	if status > 0 {
		c.Ctx.Output.SetStatus(status)
		return
	}
	prods := cookie.GetProducts(db)
	log.Println(prods)
	c.Data["json"] = prods
	c.ServeJson()
}

func (c *OneUserCtrl) GetRecipeProducts() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie, status := getCookie(db, c)
	if status > 0 {
		c.Ctx.Output.SetStatus(status)
		return
	}
	prods, err := cookie.GetProductsByRecipes(db)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println(prods)
	c.Data["json"] = prods
	c.ServeJson()
}
