package admin

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"encoding/csv"
	"github.com/astaxie/beego"
	"log"
	"net/http"
	"time"
)

type StatisticsCtrl struct {
	beego.Controller
}

func (c *StatisticsCtrl) GetTemplateStatistics() {
	c.TplNames = "admin/statistics.tpl"
}

func (c *StatisticsCtrl) GetStatistics() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()

	start_s := c.GetString("Start")
	end_s := c.GetString("End")
	start, err := time.Parse("02/01/2006", start_s)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}

	end, err := time.Parse("02/01/2006", end_s)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		return
	}

	on := c.GetString("On")

	if on == "recipes" {
		sheet := models.GetMatrixChoosenRecipes(db, start, end)
		log.Println(sheet.ToString())

		writer := csv.NewWriter(c.Controller.Ctx.ResponseWriter)
		for _, record := range sheet.ToString() {
			err := writer.Write(record)
			if err != nil {
				c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		c.Ctx.Output.Header("Content-Type", "text/csv")
		c.Ctx.Output.Header("Content-Disposition", "attachment; filename=recipes.csv")

		writer.Flush()
	} else if on == "products" {
		sheet := models.GetMatrixChoosenProducts(db, start, end)
		log.Println(sheet.ToString())

		writer := csv.NewWriter(c.Controller.Ctx.ResponseWriter)
		for _, record := range sheet.ToString() {
			err := writer.Write(record)
			if err != nil {
				c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		c.Ctx.Output.Header("Content-Type", "text/csv")
		c.Ctx.Output.Header("Content-Disposition", "attachment; filename=products.csv")

		writer.Flush()
	} else {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
	}

}
