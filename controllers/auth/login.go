package auth

import (
	"beegorecipes/models"
	"beegorecipes/utils"

	"github.com/astaxie/beego"
	"log"
)

type LoginCtrl struct {
	beego.Controller
}

func (c *LoginCtrl) Get() {
	beego.ReadFromRequest(&c.Controller)
	c.TplNames = "auth/login.tpl"
}

func (c *LoginCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	log.Println("Loged In!")
	user := new(models.User)
	user.Email = c.GetString("Email")
	user.Password = c.GetString("Password")
	err := user.Authenticate(db)
	flash := beego.NewFlash()

	if err != nil {
		log.Println("have not been found")
		flash.Error("The email or password is wrong")
		flash.Store(&c.Controller)
		c.Redirect("/auth/login", 302)
		return
	}
	c.SetSession("user_id", user.Id.Hex())
	if user.IsAdmin {
		c.SetSession("admin_id", user.Id.Hex())
	}
	log.Println("Have been found with Name " + user.Name)
	flash.Success("You have been login successfully !")
	flash.Store(&c.Controller)
	c.Redirect("/", 302)
}
