package auth

import (
	"github.com/astaxie/beego"
)

type LogoutCtrl struct {
	beego.Controller
}

func (c *LogoutCtrl) Get() {

	c.DelSession("user_id")
	c.DelSession("admin_id")
	c.DestroySession()
	c.Redirect("/", 302)
}
