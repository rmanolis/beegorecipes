package auth

import (
	"beegorecipes/models"
	"beegorecipes/utils"

	"github.com/astaxie/beego"
)

type RegisterCtrl struct {
	beego.Controller
}

type Credentials struct {
	Name     string `form:",text"`
	Email    string `form:",email"`
	Password string `form:",password"`
}

func (c *RegisterCtrl) Get() {
	c.Data["Form"] = &Credentials{}
	c.TplNames = "auth/register.tpl"
}

func (c *RegisterCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()

	user := new(models.User)
	user.Name = c.GetString("Name")
	user.Password = c.GetString("Password")
	user.Email = c.GetString("Email")
	user.IsAdmin = true
	err := user.Insert(db)

	flash := beego.NewFlash()
	if err != nil {
		flash.Error("There was a problem")
		flash.Store(&c.Controller)
		c.Redirect("/auth/register", 302)
		return
	}
	flash.Success("Registered successfully !")
	flash.Store(&c.Controller)
	c.Redirect("/auth/login", 302)
}
