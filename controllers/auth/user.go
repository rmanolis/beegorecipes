package auth

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"github.com/astaxie/beego"
)

type UserCtrl struct {
	beego.Controller
}

func (c *UserCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id, ok := c.GetSession("user_id").(string)
	if !ok || !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)

	user, err := models.GetUserById(db, bid)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	user.Password = ""
	c.Data["json"] = user
	c.ServeJson()
}
