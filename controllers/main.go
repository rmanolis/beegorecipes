package controllers

import (
	"github.com/astaxie/beego"
	"log"
)

type MainCtrl struct {
	beego.Controller
}

func (c *MainCtrl) Get() {
	beego.ReadFromRequest(&c.Controller)
	_, ok := c.GetSession("user_id").(string)
	if ok {
		log.Println("is user")
	}
	c.TplNames = "index.tpl"
}
