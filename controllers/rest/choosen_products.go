package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type ChoosenProductsCtrl struct {
	beego.Controller
}

func (c *ChoosenProductsCtrl) GetProducts() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	prods := cobj.GetProducts(db)

	c.Data["json"] = prods
	c.ServeJson()
}

func (c *ChoosenProductsCtrl) ChooseProduct() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)

	id := c.Ctx.Input.Param(":id")

	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	product := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), product)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenProduct)
	cr.CookieId = cobj.Id
	cr.ProductId = product.Id
	err = cr.Insert(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}

	c.ServeJson()
}

func (c *ChoosenProductsCtrl) RejectProduct() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	id := c.Ctx.Input.Param(":id")

	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenProduct)
	err = cobj.FindChoosenProduct(db, bson.ObjectIdHex(id), cr)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	err = cr.Remove(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}

	c.ServeJson()

}
