package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type ChoosenRecipesCtrl struct {
	beego.Controller
}

func (c *ChoosenRecipesCtrl) GetRecipes() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	recs := cobj.GetRecipes(db)
	log.Println(recs)
	recms := []map[string]interface{}{}
	for i := range recs {
		recs[i].IsChoosen = true
		recms = append(recms, getJsonRecipe(db, &recs[i]))
	}

	c.Data["json"] = recms
	c.ServeJson()
}

func (c *ChoosenRecipesCtrl) GetProducts() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	prods, _ := cobj.GetProductsByRecipes(db)
	log.Println(prods)

	for i, v := range prods {
		if cobj.IsChoosenProduct(db, v.Id) {
			prods[i].IsChoosen = true
		}
	}

	c.Data["json"] = prods
	c.ServeJson()
}

func (c *ChoosenRecipesCtrl) GetIngredients() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	ings, _ := cobj.GetIngredientsByRecipes(db)
	c.Data["json"] = ings
	c.ServeJson()

}

func (c *ChoosenRecipesCtrl) ChooseRecipe() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)

	id := c.Ctx.Input.Param(":id")

	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	recipe := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), recipe)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenRecipe)
	cr.CookieId = cobj.Id
	cr.RecipeId = recipe.Id
	err = cr.Insert(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}

	c.ServeJson()
}

func (c *ChoosenRecipesCtrl) RejectRecipe() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	id := c.Ctx.Input.Param(":id")

	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenRecipe)
	err = models.FindChoosenRecipe(db, cobj.Id, bson.ObjectIdHex(id), cr)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	err = cr.Remove(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}

	c.ServeJson()

}
