package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"net/http"

	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type CookieCtrl struct {
	beego.Controller
}

func createNewCookie(c *CookieCtrl, db *mgo.Database) {
	for {
		str := utils.RandSeq(20)
		_, err := models.FindCookie(db, str)
		if err != nil {
			cookie := new(models.Cookie)
			cookie.Cookie = str
			user_id, ok := c.Ctx.Input.Session("user_id").(string)
			if ok {
				cookie.UserId = bson.ObjectIdHex(user_id)
			}
			err = cookie.Insert(db)
			c.SetSecureCookie(utils.SECRET, "cookie_id", str)
			break
		}
	}
}

func (c *CookieCtrl) NewCookie() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()

	str := utils.GetCookie(c.Ctx)
	cookie, err := models.FindCookie(db, str)
	if !cookie.UserId.Valid() {
		user_id, ok := c.Ctx.Input.Session("user_id").(string)
		if ok {
			cookie.UserId = bson.ObjectIdHex(user_id)
		}
		cookie.Update(db)
	}

	if len(str) == 0 || err != nil {
		createNewCookie(c, db)
	}
	c.ServeJson()
}

func (c *CookieCtrl) FinishCookie() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	str := utils.GetCookie(c.Ctx)
	cookie, err := models.FindCookie(db, str)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	user_id, ok := c.Ctx.Input.Session("user_id").(string)
	if ok {
		cookie.UserId = bson.ObjectIdHex(user_id)
	}
	cookie.IsFinished = true
	cookie.Update(db)

	createNewCookie(c, db)
	c.ServeJson()
}

func (c *CookieCtrl) BuyCookie() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	str := utils.GetCookie(c.Ctx)
	cookie, err := models.FindCookie(db, str)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	user_id, ok := c.Ctx.Input.Session("user_id").(string)
	if !ok || !bson.IsObjectIdHex(user_id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	uid := bson.ObjectIdHex(user_id)
	_, err = models.GetUserById(db, uid)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	cookie.UserId = uid
	cookie.IsFinished = true
	cookie.IsForBuy = true
	cookie.Update(db)

	createNewCookie(c, db)
	c.ServeJson()

}
