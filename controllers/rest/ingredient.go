package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"net/http"

	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
)

type IngredientCtrl struct {
	beego.Controller
}

func (c *IngredientCtrl) GetIngredient() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bid, ing)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	stags := []string{}
	for _, v := range ing.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	ingm := map[string]interface{}{
		"Name":  ing.Name,
		"STags": stags,
		"Tags":  ing.Tags,
	}
	c.Data["json"] = ingm
	c.ServeJson()
}

func (c *IngredientCtrl) GetProducts() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bid, ing)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	products, err := ing.GetProducts(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	for i, v := range products {
		if cobj.IsChoosenProduct(db, v.Id) {
			products[i].IsChoosen = true
		}
	}

	c.Data["json"] = products
	c.ServeJson()
}
