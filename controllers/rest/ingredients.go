package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
	"net/http"
)

type ListIngredientsCtrl struct {
	beego.Controller
}

func (c *ListIngredientsCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	ings, _ := models.FindIngredients(db, name, size, page)
	c.Data["json"] = ings
	c.ServeJson()
}

type SearchIngredientsCtrl struct {
	beego.Controller
}

func (c *SearchIngredientsCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()

	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	strs := []string{}
	c.Ctx.Input.Bind(&strs, "Tags")
	tags := models.NamesToTags(db, strs)

	log.Println(name)
	log.Println(tags)
	ings, err := models.SearchIngredients(db, name, tags, size, page)
	if err != nil {
		log.Println(err.Error())
	}
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		log.Println(err.Error())
		return
	}

	c.Data["json"] = ings
	c.ServeJson()
}
