package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"net/http"

	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
	"log"
)

type ProductCtrl struct {
	beego.Controller
}

func (c *ProductCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	log.Println(id)
	bid := bson.ObjectIdHex(id)
	prod := new(models.Product)
	err := models.FindProductId(db, bid, prod)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	stags := []string{}
	for _, v := range prod.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	sings := []string{}
	for _, v := range prod.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings = append(sings, ing.Name)
	}
	log.Println(prod)
	prodm := map[string]interface{}{
		"Name":         prod.Name,
		"ProductType":  prod.ProductType,
		"Description":  prod.Description,
		"Price":        prod.Price,
		"STags":        stags,
		"Tags":         prod.Tags,
		"SIngredients": sings,
		"Ingredients":  prod.Ingredients,
	}
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		if cobj.IsChoosenProduct(db, prod.Id) {
			prodm["IsChoosen"] = true
		}
	}

	log.Println(prodm)
	c.Data["json"] = prodm
	c.ServeJson()
}
