package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
	"net/http"
)

type ListProductsCtrl struct {
	beego.Controller
}

func (c *ListProductsCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	prods, _ := models.FindProducts(db, name, size, page)
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		for i, v := range prods {
			if cobj.IsChoosenProduct(db, v.Id) {
				prods[i].IsChoosen = true
			}
		}
	}

	c.Data["json"] = prods
	c.ServeJson()
}

type SearchProductsCtrl struct {
	beego.Controller
}

func (c *SearchProductsCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()

	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	strs := []string{}
	c.Ctx.Input.Bind(&strs, "Tags")
	tags := models.NamesToTags(db, strs)
	c.Ctx.Input.Bind(&strs, "Ingredients")
	ings := models.NamesToIngredients(db, strs)
	log.Println(name)
	log.Println(tags)
	log.Println(ings)
	prods, err := models.SearchProducts(db, name, ings, tags, size, page)
	if err != nil {
		log.Println(err.Error())
	}
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		log.Println(err.Error())
		return
	}

	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		log.Println(err.Error())
		return
	}

	for i, v := range prods.Objects.([]models.Product) {
		if cobj.IsChoosenProduct(db, v.Id) {
			prods.Objects.([]models.Product)[i].IsChoosen = true
		}
	}

	c.Data["json"] = prods
	c.ServeJson()
}
