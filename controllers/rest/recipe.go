package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type RecipeCtrl struct {
	beego.Controller
}

func getJsonRecipe(db *mgo.Database, rec *models.Recipe) map[string]interface{} {
	stags := []string{}
	for _, v := range rec.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	sings := []string{}
	for _, v := range rec.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings = append(sings, ing.Name)
	}

	sprods := []string{}
	for _, v := range rec.Products {
		product := new(models.Product)
		models.FindProductId(db, v, product)
		sprods = append(sprods, product.Name)
	}

	recm := map[string]interface{}{
		"Id":           rec.Id,
		"Name":         rec.Name,
		"Servings":     rec.Servings,
		"Steps":        rec.Steps,
		"STags":        stags,
		"Tags":         rec.Tags,
		"SIngredients": sings,
		"Ingredients":  rec.Ingredients,
		"SProducts":    sprods,
		"Products":     rec.Products,
		"IsChoosen":    rec.IsChoosen,
	}

	return recm
}

func (c *RecipeCtrl) GetRecipe() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	log.Println(id)
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bid, rec)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	recm := getJsonRecipe(db, rec)
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)

	if err == nil {
		if cobj.IsChoosenRecipe(db, rec.Id) {
			recm["IsChoosen"] = true
		}
	}

	c.Data["json"] = recm
	c.ServeJson()
}

func (c *RecipeCtrl) GetIngredients() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bid, rec)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	ings := rec.GetIngredients(db)
	c.Data["json"] = ings
	c.ServeJson()
}
