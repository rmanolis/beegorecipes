package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
	"net/http"
)

type ListRecipesCtrl struct {
	beego.Controller
}

func (c *ListRecipesCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	recs, _ := models.FindRecipes(db, name, size, page)
	log.Println(recs)
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		for i, v := range recs {
			if cobj.IsChoosenRecipe(db, v.Id) {
				recs[i].IsChoosen = true
			}
		}
	}

	c.Data["json"] = recs
	c.ServeJson()
}

type SearchRecipesCtrl struct {
	beego.Controller
}

func (c *SearchRecipesCtrl) Post() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	strs := []string{}
	c.Ctx.Input.Bind(&strs, "Tags")
	tags := models.NamesToTags(db, strs)
	c.Ctx.Input.Bind(&strs, "Ingredients")
	ings := models.NamesToIngredients(db, strs)
	log.Println(name)
	log.Println(tags)
	log.Println(ings)
	recs, err := models.SearchRecipes(db, name, ings, tags, size, page)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println(recs)
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		log.Println(err.Error())
		return
	}

	for i, v := range recs.Objects.([]models.Recipe) {
		if cobj.IsChoosenRecipe(db, v.Id) {
			recs.Objects.([]models.Recipe)[i].IsChoosen = true
		}
	}

	c.Data["json"] = recs
	c.ServeJson()
}
