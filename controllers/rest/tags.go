package rest

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
)

type ListTagsCtrl struct {
	beego.Controller
}

func (c *ListTagsCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	name := c.GetString("Name")
	tags, _ := models.FindTags(db, name, size, page)
	c.Data["json"] = tags
	c.ServeJson()
}
