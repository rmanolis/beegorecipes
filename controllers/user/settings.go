package user

import (
	"beegorecipes/utils"
	"beegorecipes/utils/chelpers"

	"github.com/astaxie/beego"
	"net/http"
)

type UserSettingsCtrl struct {
	beego.Controller
}

func (c *UserSettingsCtrl) GetPageSettings() {
	c.TplNames = "user/settings.tpl"

}

func (c *UserSettingsCtrl) EditPassword() {

	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	beego.ReadFromRequest(&c.Controller)
	password := c.GetString("Password")
	rpassword := c.GetString("RepeatPassword")

	user, status := chelpers.CheckUser(db, c.Controller)

	if status > 0 {
		c.Ctx.ResponseWriter.WriteHeader(status)
		return
	}
	if password != rpassword {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotAcceptable)
		c.Ctx.ResponseWriter.Write([]byte("the passwords are not equal"))
		return
	}

	user.SetPassword(db, password)
	err := user.Update(db)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}
	c.ServeJson()
}
