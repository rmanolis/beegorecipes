package visitor

import (
	"github.com/astaxie/beego"
)

type ChoosenRecipesCtrl struct {
	beego.Controller
}

func (c *ChoosenRecipesCtrl) Get() {
	c.TplNames = "visitor/choosen_recipes.tpl"

}
