package visitor

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"net/http"
)

type PdfCtrl struct {
	beego.Controller
}

func (c *PdfCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	cookie := utils.GetCookie(c.Ctx)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}
	cobj.IsPDFed = true
	cobj.Update(db)
	c.Data["products"] = cobj.GetProducts(db)
	c.Data["recipes"] = cobj.GetRecipes(db)
	c.Data["ingredients"], _ = cobj.GetIngredientsByRecipes(db)
	c.TplNames = "visitor/pdf.tpl"
}
