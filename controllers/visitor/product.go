package visitor

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type ProductCtrl struct {
	beego.Controller
}

func (c *ProductCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	prod := new(models.Product)
	err := models.FindProductId(db, bid, prod)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	stags := ""
	for _, v := range prod.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags += tag.Name + ", "
	}

	sings := ""
	for _, v := range prod.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings += ing.Name + ", "
	}

	c.Data["name"] = prod.Name
	c.Data["description"] = prod.Description
	c.Data["price"] = prod.Price
	c.Data["tags"] = stags
	c.Data["ingredients"] = sings

	log.Println(prod)
	c.TplNames = "visitor/product.tpl"

}
