package visitor

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
)

type ListProductsCtrl struct {
	beego.Controller
}

func (c *ListProductsCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	beego.ReadFromRequest(&c.Controller)
	lo, err := models.GetProducts(db, size, page)

	if err != nil {
		log.Println(err.Error())
	}
	c.Data["paginator"] = utils.NewPaginator(c.Ctx.Request, size, lo.Total)
	c.Data["products"] = lo.Objects.([]models.Product)

	c.TplNames = "visitor/products.tpl"
}
