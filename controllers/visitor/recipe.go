package visitor

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type RecipeCtrl struct {
	beego.Controller
}

func (c *RecipeCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	id := c.Ctx.Input.Param(":id")
	if !bson.IsObjectIdHex(id) {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bid, rec)
	if err != nil {
		c.Ctx.ResponseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	stags := ""
	for _, v := range rec.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags += tag.Name + ", "
	}

	sings := ""
	for _, v := range rec.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings += ing.Name + ", "
	}

	c.Data["name"] = rec.Name
	c.Data["servings"] = rec.Servings
	c.Data["execution"] = rec.Steps
	c.Data["tags"] = stags
	c.Data["ingredients"] = sings

	log.Println(rec)
	c.TplNames = "visitor/recipe.tpl"

}
