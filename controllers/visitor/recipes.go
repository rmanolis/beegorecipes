package visitor

import (
	"beegorecipes/models"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"log"
)

type ListRecipesCtrl struct {
	beego.Controller
}

func (c *ListRecipesCtrl) Get() {
	db, ses := utils.GetDB(c.Ctx)
	defer ses.Close()
	size, page := utils.SizePage(&c.Controller)
	beego.ReadFromRequest(&c.Controller)
	lo, err := models.GetRecipes(db, size, page)

	if err != nil {
		log.Println(err.Error())
	}
	c.Data["paginator"] = utils.NewPaginator(c.Ctx.Request, size, lo.Total)
	c.Data["recipes"] = lo.Objects.([]models.Recipe)

	c.TplNames = "visitor/recipes.tpl"
}
