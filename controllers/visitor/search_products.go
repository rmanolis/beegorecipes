package visitor

import (
	"github.com/astaxie/beego"
)

type SearchProductsCtrl struct {
	beego.Controller
}

func (c *SearchProductsCtrl) Get() {
	c.TplNames = "visitor/search_products.tpl"

}
