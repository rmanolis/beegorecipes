package visitor

import (
	"github.com/astaxie/beego"
)

type SearchRecipesCtrl struct {
	beego.Controller
}

func (c *SearchRecipesCtrl) Get() {
	c.TplNames = "visitor/search_recipes.tpl"

}
