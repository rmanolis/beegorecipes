package main

import (
	_ "beegorecipes/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.ViewsPath = "views"
	beego.SetStaticPath("/static", "static")
	beego.MaxMemory = 6 * 1024 * 1024
	beego.TemplateLeft = "{%"
	beego.TemplateRight = "%}"
	beego.Run()

}
