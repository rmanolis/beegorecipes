package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type ChoosenProduct struct {
	Id        bson.ObjectId ` bson:"_id,omitempty"`
	CookieId  bson.ObjectId
	ProductId bson.ObjectId
	Date      time.Time
}

func (ch *ChoosenProduct) Insert(db *mgo.Database) error {
	c := db.C(CHOOSEN_PRODUCT)
	ch.Id = bson.NewObjectId()
	ch.Date = time.Now()
	return c.Insert(ch)
}

func (ch *ChoosenProduct) Remove(db *mgo.Database) error {
	c := db.C(CHOOSEN_PRODUCT)
	return c.Remove(ch)
}

func (ch *ChoosenProduct) Product(db *mgo.Database, prod *Product) error {
	return FindProductId(db, ch.ProductId, prod)
}

func (c *Cookie) GetProducts(db *mgo.Database) []Product {
	ccr := db.C(CHOOSEN_PRODUCT)
	crls := []ChoosenProduct{}
	ccr.Find(bson.M{"cookieid": c.Id}).All(&crls)
	prods := []Product{}
	for _, v := range crls {
		prod := new(Product)
		err := FindProductId(db, v.ProductId, prod)
		if err == nil {
			prods = append(prods, *prod)
		}
	}
	return prods
}

func (c *Cookie) IsChoosenProduct(db *mgo.Database, pid bson.ObjectId) bool {
	ccr := db.C(CHOOSEN_PRODUCT)
	n, _ := ccr.Find(bson.M{"cookieid": c.Id, "productid": pid}).Count()
	return n > 0
}

func (cookie *Cookie) FindChoosenProduct(db *mgo.Database, pid bson.ObjectId, cr *ChoosenProduct) error {
	c := db.C(CHOOSEN_PRODUCT)
	err := c.Find(bson.M{"productid": pid,
		"cookieid": cookie.Id}).One(&cr)
	return err
}
