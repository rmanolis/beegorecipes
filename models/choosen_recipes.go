package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type ChoosenRecipe struct {
	Id       bson.ObjectId ` bson:"_id,omitempty"`
	CookieId bson.ObjectId
	RecipeId bson.ObjectId
	Date     time.Time
}

func (ch *ChoosenRecipe) Insert(db *mgo.Database) error {
	c := db.C(CHOOSEN_RECIPE)
	ch.Id = bson.NewObjectId()
	ch.Date = time.Now()
	return c.Insert(ch)
}

func (ch *ChoosenRecipe) Remove(db *mgo.Database) error {
	c := db.C(CHOOSEN_RECIPE)
	return c.Remove(ch)
}

func (ch *ChoosenRecipe) Recipe(db *mgo.Database, rec *Recipe) error {
	return FindRecipeId(db, ch.RecipeId, rec)
}

func (c *Cookie) GetRecipes(db *mgo.Database) []Recipe {
	ccr := db.C(CHOOSEN_RECIPE)
	crls := []ChoosenRecipe{}
	ccr.Find(bson.M{"cookieid": c.Id}).All(&crls)
	recs := []Recipe{}
	for _, v := range crls {
		rec := new(Recipe)
		err := FindRecipeId(db, v.RecipeId, rec)
		if err == nil {
			recs = append(recs, *rec)
		}
	}
	return recs
}

func (c *Cookie) IsChoosenRecipe(db *mgo.Database, rid bson.ObjectId) bool {
	ccr := db.C(CHOOSEN_RECIPE)
	n, _ := ccr.Find(bson.M{"cookieid": c.Id, "recipeid": rid}).Count()
	return n > 0
}

func FindChoosenRecipe(db *mgo.Database, cid, rid bson.ObjectId, cr *ChoosenRecipe) error {
	c := db.C(CHOOSEN_RECIPE)
	err := c.Find(bson.M{"recipeid": rid,
		"cookieid": cid}).One(&cr)
	return err
}
