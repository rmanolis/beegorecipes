package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Cookie struct {
	Id         bson.ObjectId ` bson:"_id,omitempty"`
	Cookie     string
	UserId     bson.ObjectId ` bson:",omitempty"`
	IsFinished bool
	IsForBuy   bool
	IsPDFed    bool
	Name       string
	Date       time.Time
}

func (cookie *Cookie) Insert(db *mgo.Database) error {
	c := db.C(COOKIE)
	cookie.Id = bson.NewObjectId()
	cookie.Date = time.Now()
	return c.Insert(cookie)
}

func (cookie *Cookie) Update(db *mgo.Database) error {
	c := db.C(COOKIE)
	return c.UpdateId(cookie.Id, cookie)
}

func (cobj *Cookie) GetIngredientsByRecipes(db *mgo.Database) ([]Ingredient, error) {
	c := db.C(INGREDIENT)
	recs := cobj.GetRecipes(db)
	ingids := []bson.ObjectId{}
	for _, v := range recs {
		ingids = append(ingids, v.Ingredients...)
	}
	ings := []Ingredient{}
	err := c.Find(bson.M{"_id": bson.M{"$in": ingids}}).All(&ings)
	return ings, err

}

func (cobj *Cookie) GetProductsByRecipes(db *mgo.Database) ([]Product, error) {
	c := db.C(PRODUCT)
	recs := cobj.GetRecipes(db)
	productids := []bson.ObjectId{}
	for _, v := range recs {
		productids = append(productids, v.Products...)
	}
	products := []Product{}
	err := c.Find(bson.M{"_id": bson.M{"$in": productids}}).All(&products)
	return products, err

}

func FindCookie(db *mgo.Database, name string) (*Cookie, error) {
	c := db.C(COOKIE)
	cookie := new(Cookie)
	err := c.Find(bson.M{"cookie": name}).One(&cookie)
	return cookie, err
}

func FindCookieId(db *mgo.Database, id bson.ObjectId) (*Cookie, error) {
	c := db.C(COOKIE)
	cookie := new(Cookie)
	err := c.FindId(id).One(&cookie)
	return cookie, err
}

func SearchCookies(db *mgo.Database, name string, is_user, is_finished bool,
	size, page int) (*ListObjects, error) {
	c := db.C(COOKIE)
	cookies := []Cookie{}
	skip := size * page
	query := bson.M{}
	if len(name) > 0 {
		query["name"] = bson.M{"$regex": name}
	}
	if !is_user {
		query["userid"] = bson.M{"$eq": nil}
	} else {
		query["userid"] = bson.M{"$ne": nil}
	}
	query["isfinished"] = bson.M{"$eq": is_finished}
	err := c.Find(query).
		Skip(skip).Limit(size).Sort("-date").All(&cookies)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, cookies)
	return lo, err
}
