package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Ingredient struct {
	Id     bson.ObjectId ` bson:"_id,omitempty"`
	Name   string
	Tags   []bson.ObjectId
	Photos []bson.ObjectId
}

func NamesToIngredients(db *mgo.Database, names []string) []bson.ObjectId {
	ing_ids := []bson.ObjectId{}
	for _, v := range names {
		ing := new(Ingredient)
		err := FindIngredient(db, v, ing)
		if err == nil {
			ing_ids = append(ing_ids, ing.Id)
		}
	}
	return ing_ids
}

func SearchIngredients(db *mgo.Database, name string,
	tags []bson.ObjectId,
	size, page int) (*ListObjects, error) {
	c := db.C(INGREDIENT)
	ings := []Ingredient{}
	skip := size * page
	query := bson.M{}
	if len(tags) > 0 {
		query["tags"] = bson.M{"$all": tags}
	}
	if len(name) > 0 {
		query["name"] = bson.M{"$regex": name}
	}
	err := c.Find(query).
		Skip(skip).Limit(size).All(&ings)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, ings)
	return lo, err
}

func (ing *Ingredient) Insert(db *mgo.Database) error {
	c := db.C(INGREDIENT)
	return c.Insert(ing)
}

func GetIngredients(db *mgo.Database, page, size int) (*ListObjects, error) {
	c := db.C(INGREDIENT)
	ings := []Ingredient{}
	skip := size * page
	err := c.Find(bson.M{}).Skip(skip).Limit(size).All(&ings)
	total, _ := c.Find(bson.M{}).Count()
	lo := GetListObjects(size, page, total, ings)
	return lo, err
}

func GetIngredientsByTags(db *mgo.Database, tags []bson.ObjectId) ([]Ingredient, error) {
	c := db.C(INGREDIENT)
	ingredients := []Ingredient{}
	err := c.Find(bson.M{"tags": bson.M{"$all": tags}}).All(&ingredients)
	return ingredients, err
}

func FindIngredientId(db *mgo.Database, id bson.ObjectId, ing *Ingredient) error {
	c := db.C(INGREDIENT)
	err := c.FindId(id).One(&ing)
	return err
}

func FindIngredient(db *mgo.Database, name string, ing *Ingredient) error {
	c := db.C(INGREDIENT)
	err := c.Find(bson.M{"name": name}).One(&ing)
	return err
}

func FindIngredients(db *mgo.Database, name string, page, size int) ([]Ingredient, error) {
	c := db.C(INGREDIENT)
	ings := []Ingredient{}
	skip := size * page
	err := c.Find(bson.M{"name": bson.M{"$regex": name}}).Skip(skip).Limit(size).All(&ings)
	return ings, err

}

func ListIngredientsByRecipes(db *mgo.Database, recipes []Recipe) ([]Ingredient, error) {
	ings := map[bson.ObjectId]bool{}
	for _, recipe := range recipes {
		for _, ingredient := range recipe.Ingredients {
			_, ok := ings[ingredient]
			if !ok {
				ings[ingredient] = true
			}
		}
	}

	ing_ids := []bson.ObjectId{}
	for k := range ings {
		ing_ids = append(ing_ids, k)
	}

	c := db.C(INGREDIENT)
	ingredients := []Ingredient{}
	err := c.Find(bson.M{"_id": bson.M{"$in": ing_ids}}).All(&ingredients)
	return ingredients, err
}

func (ing *Ingredient) AddTag(db *mgo.Database, tag bson.ObjectId) error {
	c := db.C(INGREDIENT)
	ing.Tags = append(ing.Tags, tag)
	return c.UpdateId(ing.Id, ing)
}

func (ing *Ingredient) RemoveTag(db *mgo.Database, tag bson.ObjectId) error {
	c := db.C(INGREDIENT)
	for i, _ := range ing.Tags {
		if ing.Tags[i] == tag {
			ing.Tags = append(ing.Tags[:i], ing.Tags[i+1:]...)
			break
		}
	}
	return c.UpdateId(ing.Id, ing)
}

func (ing *Ingredient) GetTags(db *mgo.Database) []Tag {
	tags := []Tag{}
	for _, v := range ing.Tags {
		tag := new(Tag)
		err := FindTagId(db, v, tag)
		if err == nil {
			tags = append(tags, *tag)
		}
	}
	return tags
}

func (ing *Ingredient) Remove(db *mgo.Database) error {
	c := db.C(INGREDIENT)
	return c.RemoveId(ing.Id)
}

func (ing *Ingredient) Update(db *mgo.Database) error {
	c := db.C(INGREDIENT)
	return c.UpdateId(ing.Id, ing)
}

func (ing *Ingredient) GetProducts(db *mgo.Database) ([]Product, error) {
	c := db.C(PRODUCT)
	products := []Product{}
	err := c.Find(bson.M{"producttype": INGREDIENT_TYPE, "ingredients": bson.M{"$all": []bson.ObjectId{ing.Id}}}).All(&products)
	return products, err
}
