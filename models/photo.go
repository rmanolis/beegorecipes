package models

import (
	//"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Photo struct {
	Id       bson.ObjectId ` bson:"_id,omitempty"`
	Filename string
	Type     string
}
