package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const (
	INGREDIENT_TYPE = "ingredient"
	DRINK_TYPE      = "drink"
	DESERT_TYPE     = "desert"
	SIDE_DISH_TYPE  = "side_dish"
	TOOL            = "tool"
)

type Product struct {
	Id          bson.ObjectId ` bson:"_id,omitempty"`
	Name        string
	Description string
	Price       float64
	ProductType string
	Tags        []bson.ObjectId
	Ingredients []bson.ObjectId
	Photos      []bson.ObjectId
	IsChoosen   bool //not for saving
	Date        time.Time
}

func NamesToProducts(db *mgo.Database, names []string) []bson.ObjectId {
	prod_ids := []bson.ObjectId{}
	for _, v := range names {
		product := new(Product)
		err := FindProduct(db, v, product)
		if err == nil {
			prod_ids = append(prod_ids, product.Id)
		}
	}
	return prod_ids
}

func SearchProducts(db *mgo.Database, name string,
	ingredients []bson.ObjectId,
	tags []bson.ObjectId,
	size, page int) (*ListObjects, error) {
	c := db.C(PRODUCT)
	prods := []Product{}
	skip := size * page
	query := bson.M{}
	if len(ingredients) > 0 {
		query["ingredients"] = bson.M{"$all": ingredients}
	}
	if len(tags) > 0 {
		query["tags"] = bson.M{"$all": tags}
	}
	if len(name) > 0 {
		query["name"] = bson.M{"$regex": name, "$options": "i"}
	}
	err := c.Find(query).
		Skip(skip).Limit(size).All(&prods)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, prods)
	return lo, err
}

func (product *Product) Insert(db *mgo.Database) error {
	c := db.C(PRODUCT)
	product.Date = time.Now()
	return c.Insert(product)
}

func (product *Product) Update(db *mgo.Database) error {
	c := db.C(PRODUCT)
	return c.UpdateId(product.Id, product)
}

func GetProducts(db *mgo.Database, size, page int) (*ListObjects, error) {
	c := db.C(PRODUCT)
	products := []Product{}
	skip := size * page
	err := c.Find(bson.M{}).Sort("-date").Skip(skip).Limit(size).All(&products)
	total, _ := c.Find(bson.M{}).Count()
	lo := GetListObjects(size, page, total, products)
	return lo, err
}

func FindProducts(db *mgo.Database, name string, size, page int) ([]Product, error) {
	c := db.C(PRODUCT)
	products := []Product{}
	skip := size * page
	err := c.Find(bson.M{"name": bson.M{"$regex": name}}).Sort("-date").Skip(skip).Limit(size).All(&products)
	return products, err

}

func FindProduct(db *mgo.Database, name string, prod *Product) error {
	c := db.C(PRODUCT)
	err := c.Find(bson.M{"name": name}).One(&prod)
	return err
}

func FindProductId(db *mgo.Database, id bson.ObjectId, prod *Product) error {
	c := db.C(PRODUCT)
	err := c.FindId(id).One(&prod)
	return err
}

func (product *Product) Remove(db *mgo.Database) error {
	c := db.C(PRODUCT)
	return c.RemoveId(product.Id)
}

func GetProductsByTags(db *mgo.Database, tags []bson.ObjectId, size, page int) ([]Product, error) {
	c := db.C(PRODUCT)
	products := []Product{}
	skip := size * page
	err := c.Find(bson.M{"tags": bson.M{"$all": tags}}).
		Skip(skip).Limit(size).All(&products)
	return products, err
}

func GetProductsByIngredients(db *mgo.Database, ingredients []bson.ObjectId,
	size, page int) ([]Product, error) {
	c := db.C(PRODUCT)
	products := []Product{}
	skip := size * page
	err := c.Find(bson.M{"ingredients": bson.M{"$all": ingredients}}).
		Skip(skip).Limit(size).All(&products)
	return products, err
}
