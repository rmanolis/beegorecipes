package models

import (
	//"github.com/tealeg/xlsx"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"strconv"
	"time"
)

func isBetween(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

func createDays(start, end time.Time) []time.Time {
	day := time.Duration(24 * time.Hour)
	days := []time.Time{}
	end = end.Add(2 * day)
	for {
		next := start.Add(day)
		if isBetween(start, end, next) {
			days = append(days, next)
			start = next
		} else {
			break
		}
	}
	return days
}

func getChoosenRecipesByDate(db *mgo.Database, start, end time.Time) (map[bson.ObjectId]int, error) {
	c := db.C(CHOOSEN_RECIPE)
	ls := []ChoosenRecipe{}
	err := c.Find(bson.M{"$and": []bson.M{bson.M{"date": bson.M{"$gte": start}}, bson.M{"date": bson.M{"$lte": end}}}}).All(&ls)
	res := map[bson.ObjectId]int{}
	for _, v := range ls {
		rec := new(Recipe)
		err := v.Recipe(db, rec)
		if err == nil {
			_, ok := res[rec.Id]
			if ok {
				res[rec.Id] += 1
			} else {
				res[rec.Id] = 0
			}
		}
	}
	return res, err
}

func getChoosenProductsByDate(db *mgo.Database, start, end time.Time) (map[bson.ObjectId]int, error) {
	c := db.C(CHOOSEN_PRODUCT)
	ls := []ChoosenProduct{}
	err := c.Find(bson.M{"$and": []bson.M{bson.M{"date": bson.M{"$gte": start}}, bson.M{"date": bson.M{"$lte": end}}}}).All(&ls)
	res := map[bson.ObjectId]int{}
	for _, v := range ls {
		prod := new(Product)
		err := v.Product(db, prod)
		if err == nil {
			_, ok := res[prod.Id]
			if ok {
				res[prod.Id] += 1
			} else {
				res[prod.Id] = 1
			}
		}
	}
	return res, err
}

type Columns map[bson.ObjectId]int
type Items struct {
	SetIds map[bson.ObjectId]bool
	Dates  []time.Time
	Result map[time.Time]Columns //string is time
}

func GetChoosenRecipeByDay(db *mgo.Database, start, end time.Time) Items {
	days := createDays(start, end)
	items := Items{
		SetIds: map[bson.ObjectId]bool{},
		Dates:  []time.Time{},
		Result: map[time.Time]Columns{},
	}
	t_start := days[0]
	for i := 1; i < len(days); i++ {
		t_end := days[i]
		crecs, err := getChoosenRecipesByDate(db, t_start, t_end)
		if err != nil {
			log.Println(err.Error())
		}
		items.Result[t_start] = crecs
		setList(getKeys(crecs), items.SetIds)
		items.Dates = append(items.Dates, t_start)
		t_start = t_end
	}
	return items
}

func GetChoosenProductByDay(db *mgo.Database, start, end time.Time) Items {
	days := createDays(start, end)
	items := Items{
		SetIds: map[bson.ObjectId]bool{},
		Dates:  []time.Time{},
		Result: map[time.Time]Columns{},
	}
	t_start := days[0]
	for i := 1; i < len(days); i++ {
		t_end := days[i]
		cprods, err := getChoosenProductsByDate(db, t_start, t_end)
		if err != nil {
			log.Println(err.Error())
		}
		items.Result[t_start] = cprods
		setList(getKeys(cprods), items.SetIds)
		items.Dates = append(items.Dates, t_start)
		t_start = t_end
	}
	return items
}

func getKeys(o interface{}) []bson.ObjectId {
	ls := []bson.ObjectId{}
	switch objs := o.(type) {
	case map[bson.ObjectId]int:
		for k := range objs {
			ls = append(ls, k)
		}

	case map[bson.ObjectId]bool:
		for k := range objs {
			ls = append(ls, k)
		}

	}
	return ls
}

func setList(keys []bson.ObjectId, setter map[bson.ObjectId]bool) {
	for _, v := range keys {
		setter[v] = false
	}
}

type Sheet struct {
	Matrix  [][]int
	Columns []string
	Rows    []string
}

func (sh *Sheet) ToString() [][]string {
	str_matrix := make([][]string, len(sh.Rows)+1)
	log.Println(sh.Matrix)
	str_matrix[0] = append(str_matrix[0], "")
	str_matrix[0] = append(str_matrix[0], sh.Columns...)
	for k, name := range sh.Rows {
		ls := []string{}
		for _, n := range sh.Matrix[k] {
			ls = append(ls, strconv.Itoa(n))
		}
		str_matrix[k+1] = append(str_matrix[k+1], name)
		str_matrix[k+1] = append(str_matrix[k+1], ls...)
	}
	return str_matrix
}

func GetMatrixChoosenRecipes(db *mgo.Database, start, end time.Time) *Sheet {
	items := GetChoosenRecipeByDay(db, start, end)
	rec_rows := map[bson.ObjectId]int{}
	cols := map[time.Time]int{}
	for k, v := range items.Dates {
		cols[v] = k
	}
	for k, v := range getKeys(items.SetIds) {
		rec_rows[v] = k
	}

	rec_matrix := make([][]int, len(rec_rows))
	for i := 0; i < len(rec_matrix); i++ {
		rec_matrix[i] = make([]int, len(cols))
	}
	for k, v := range items.Result {
		colid := cols[k]
		for krec, vrec := range v {
			rowid := rec_rows[krec]
			rec_matrix[rowid][colid] = vrec
		}
	}

	rec_sheet := new(Sheet)
	rec_sheet.Matrix = rec_matrix
	rec_sheet.Rows = make([]string, len(rec_rows))
	for k, v := range rec_rows {
		rec := new(Recipe)
		err := FindRecipeId(db, k, rec)
		if err == nil {
			rec_sheet.Rows[v] = rec.Name
		}
	}
	rec_sheet.Columns = make([]string, len(cols)+1)
	for k, v := range cols {
		rec_sheet.Columns[v] = k.Format("02/01/2006")
	}
	return rec_sheet
}

func GetMatrixChoosenProducts(db *mgo.Database, start, end time.Time) *Sheet {
	items := GetChoosenProductByDay(db, start, end)
	prod_rows := map[bson.ObjectId]int{}
	cols := map[time.Time]int{}
	for k, v := range items.Dates {
		cols[v] = k
	}
	for k, v := range getKeys(items.SetIds) {
		prod_rows[v] = k
	}
	prod_matrix := make([][]int, len(prod_rows))
	for i := 0; i < len(prod_matrix); i++ {
		prod_matrix[i] = make([]int, len(cols))
	}

	for k, v := range items.Result {
		colid := cols[k]
		for kprod, vprod := range v {
			rowid := prod_rows[kprod]
			prod_matrix[rowid][colid] = vprod
		}
	}

	prod_sheet := new(Sheet)
	prod_sheet.Matrix = prod_matrix
	prod_sheet.Rows = make([]string, len(prod_rows))
	for k, v := range prod_rows {
		prod := new(Product)
		err := FindProductId(db, k, prod)
		if err == nil {
			prod_sheet.Rows[v] = prod.Name
		}
	}
	prod_sheet.Columns = make([]string, len(cols)+1)
	for k, v := range cols {
		prod_sheet.Columns[v] = k.Format("02/01/2006")
	}
	return prod_sheet
}
