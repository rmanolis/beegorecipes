package models

const (
	TAG             = "tag"
	RECIPE          = "recipe"
	INGREDIENT      = "ingredient"
	USER            = "user"
	PRODUCT         = "product"
	COOKIE          = "cookie"
	CART            = "cart"
	CHOOSEN_RECIPE  = "choosenrecipe"
	CHOOSEN_PRODUCT = "choosenproduct"
)
