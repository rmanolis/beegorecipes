package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Tag struct {
	Id   bson.ObjectId ` bson:"_id,omitempty"`
	Name string
}

func NamesToTags(db *mgo.Database, names []string) []bson.ObjectId {
	tag_ids := []bson.ObjectId{}
	for _, v := range names {
		tag := new(Tag)
		err := FindTag(db, v, tag)
		if err == nil {
			tag_ids = append(tag_ids, tag.Id)
		}
	}
	return tag_ids
}

func (tag *Tag) Insert(db *mgo.Database) error {
	c := db.C(TAG)
	tag.Id = bson.NewObjectId()
	return c.Insert(tag)
}

func GetTags(db *mgo.Database, size, page int) (*ListObjects, error) {
	c := db.C(TAG)
	tags := []Tag{}
	skip := size * page
	err := c.Find(bson.M{}).Skip(skip).Limit(size).All(&tags)
	total, _ := c.Find(bson.M{}).Count()
	lo := GetListObjects(size, page, total, tags)
	return lo, err
}

func FindTag(db *mgo.Database, name string, tag *Tag) error {
	c := db.C(TAG)
	err := c.Find(bson.M{"name": name}).One(&tag)
	return err
}

func FindTags(db *mgo.Database, name string, size, page int) ([]Tag, error) {
	c := db.C(TAG)
	tags := []Tag{}
	skip := size * page
	err := c.Find(bson.M{"name": bson.M{"$regex": name}}).Skip(skip).Limit(size).All(&tags)
	return tags, err

}

func FindTagId(db *mgo.Database, id bson.ObjectId, tag *Tag) error {
	c := db.C(TAG)
	err := c.FindId(id).One(&tag)
	return err
}

func TagIdExists(db *mgo.Database, id bson.ObjectId) bool {
	c := db.C(TAG)
	n, _ := c.FindId(id).Count()
	return (n > 0)

}

func (tag *Tag) Remove(db *mgo.Database) error {
	c := db.C(TAG)
	return c.RemoveId(tag.Id)
}

func (tag *Tag) Update(db *mgo.Database) error {
	c := db.C(TAG)
	return c.UpdateId(tag.Id, tag)
}
