package models

import (
	"crypto/md5"
	"encoding/hex"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io"
)

type User struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Name     string
	Email    string
	Password string
	IsAdmin  bool
}

func GetUserById(db *mgo.Database, id bson.ObjectId) (*User, error) {
	user := new(User)
	err := db.C(USER).FindId(id).One(&user)
	return user, err
}

func (u *User) Insert(db *mgo.Database) error {
	c := db.C(USER)
	u.Id = bson.NewObjectId()
	h := md5.New()
	io.WriteString(h, u.Password)
	u.Password = hex.EncodeToString(h.Sum(nil))
	return c.Insert(u)
}

func SearchUsers(db *mgo.Database, name, email string,
	size, page int) (*ListObjects, error) {
	c := db.C(USER)
	users := []User{}
	skip := size * page
	query := bson.M{}
	if len(name) > 0 {
		query["name"] = bson.M{"$regex": name}
	}
	if len(email) > 0 {
		query["email"] = bson.M{"$regex": email}
	}

	err := c.Find(query).
		Skip(skip).Limit(size).All(&users)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, users)
	return lo, err
}

func (u *User) GetCookies(db *mgo.Database, size, page int) (*ListObjects, error) {
	c := db.C(COOKIE)
	cookies := []Cookie{}
	skip := size * page
	err := c.Find(bson.M{"userid": u.Id}).
		Skip(skip).Limit(size).Sort("-date").All(&cookies)
	total, _ := c.Find(bson.M{"userid": u.Id}).Count()
	lo := GetListObjects(size, page, total, cookies)
	return lo, err
}

func FindUserByEmail(db *mgo.Database, email string) (*User, error) {
	c := db.C(USER)
	u := new(User)
	err := c.Find(bson.M{
		"email": email,
	}).One(&u)
	return u, err
}

func (u *User) Authenticate(db *mgo.Database) error {
	h := md5.New()
	io.WriteString(h, u.Password)
	hex_password := hex.EncodeToString(h.Sum(nil))
	err := db.C(USER).Find(bson.M{
		"password": hex_password,
		"email":    u.Email,
	}).One(&u)
	return err
}

func (u *User) SetPassword(db *mgo.Database, pass string) error {
	new_h := md5.New()
	io.WriteString(new_h, pass)
	new_hex_password := hex.EncodeToString(new_h.Sum(nil))
	u.Password = new_hex_password
	return nil
}

func (u *User) Update(db *mgo.Database) error {
	c := db.C(USER)
	return c.UpdateId(u.Id, u)
}
