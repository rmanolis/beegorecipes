package routers

import (
	"beegorecipes/controllers/admin"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
)

func adminRouter() {
	beego.InsertFilter("/admin/*", beego.BeforeRouter, utils.AdminOnlyFilter)

	beego.Router("/admin/tags", &admin.ListTagsCtrl{})
	beego.Router("/admin/tags/:id:string", &admin.OneTagCtrl{})
	beego.Router("/admin/ingredients", &admin.ListIngredientsCtrl{})
	beego.Router("/admin/ingredients/:id:string", &admin.OneIngredientCtrl{})
	beego.Router("/admin/recipes", &admin.ListRecipesCtrl{})
	beego.Router("/admin/recipes/:id:string", &admin.OneRecipeCtrl{})
	beego.Router("/admin/products/", &admin.ListProductsCtrl{})
	beego.Router("/admin/products/:id:string", &admin.OneProductCtrl{})
	beego.Router("/admin/photo", &admin.OnePhotoCtrl{})
	beego.Router("/admin/users", &admin.ListUsersCtrl{}, "get:GetTemplateUsers")
	beego.Router("/admin/users/:id", &admin.OneUserCtrl{}, "get:GetPage")
	beego.Router("/admin/users/:id/cookies", &admin.OneUserCtrl{}, "get:GetUserCookies")
	beego.Router("/admin/users/:id/ingredients", &admin.OneUserCtrl{}, "post:GetUserIngredients")
	beego.Router("/admin/users/:id/products", &admin.OneUserCtrl{}, "post:GetUserProducts")
	beego.Router("/admin/users/:id/recipes/products", &admin.OneUserCtrl{}, "post:GetRecipeProducts")
	beego.Router("/admin/search/users/", &admin.ListUsersCtrl{}, "post:SearchUsers")
	beego.Router("/admin/cookies", &admin.ListCookiesCtrl{}, "get:GetTemplateCookies")
	beego.Router("/admin/search/cookies/", &admin.ListCookiesCtrl{}, "post:SearchCookies")

	beego.Router("/admin/statistics", &admin.StatisticsCtrl{}, "get:GetTemplateStatistics")
	beego.Router("/admin/search/statistics", &admin.StatisticsCtrl{}, "get:GetStatistics")

}
