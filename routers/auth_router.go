package routers

import (
	"beegorecipes/controllers/auth"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
)

func authRouter() {
	beego.InsertFilter("/auth/login", beego.BeforeExec, utils.NotUserOnlyFilter)
	beego.InsertFilter("/auth/register", beego.BeforeExec, utils.NotUserOnlyFilter)
	beego.InsertFilter("/auth/logout", beego.BeforeRouter, utils.UserOnlyFilter)

	beego.Router("/auth/login", &auth.LoginCtrl{})
	beego.Router("/auth/register", &auth.RegisterCtrl{})
	beego.Router("/auth/logout", &auth.LogoutCtrl{})
	beego.Router("/auth/user", &auth.UserCtrl{})

}
