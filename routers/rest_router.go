package routers

import (
	"beegorecipes/controllers/rest"
	"github.com/astaxie/beego"
)

func restRouter() {
	beego.Router("/rest/tags", &rest.ListTagsCtrl{})

	beego.Router("/rest/search/ingredients", &rest.SearchIngredientsCtrl{})
	beego.Router("/rest/ingredients", &rest.ListIngredientsCtrl{})
	beego.Router("/rest/ingredients/:id", &rest.IngredientCtrl{}, "get:GetIngredient")
	beego.Router("/rest/ingredients/:id/products", &rest.IngredientCtrl{}, "get:GetProducts")

	beego.Router("/rest/search/recipes", &rest.SearchRecipesCtrl{})
	beego.Router("/rest/recipes", &rest.ListRecipesCtrl{})
	beego.Router("/rest/recipes/:id", &rest.RecipeCtrl{}, "get:GetRecipe")
	beego.Router("/rest/recipes/:id/ingredients", &rest.RecipeCtrl{}, "get:GetIngredients")

	beego.Router("/rest/choosen/recipes", &rest.ChoosenRecipesCtrl{}, "get:GetRecipes")
	beego.Router("/rest/choosen/recipes/products", &rest.ChoosenRecipesCtrl{}, "get:GetProducts")
	beego.Router("/rest/choosen/recipes/ingredients", &rest.ChoosenRecipesCtrl{}, "get:GetIngredients")
	beego.Router("/rest/choosen/recipes/:id", &rest.ChoosenRecipesCtrl{}, "post:ChooseRecipe")
	beego.Router("/rest/choosen/recipes/:id", &rest.ChoosenRecipesCtrl{}, "delete:RejectRecipe")

	beego.Router("/rest/choosen/products", &rest.ChoosenProductsCtrl{}, "get:GetProducts")
	beego.Router("/rest/choosen/products/:id", &rest.ChoosenProductsCtrl{}, "post:ChooseProduct")
	beego.Router("/rest/choosen/products/:id", &rest.ChoosenProductsCtrl{}, "delete:RejectProduct")

	beego.Router("/rest/products", &rest.ListProductsCtrl{})
	beego.Router("/rest/search/products", &rest.SearchProductsCtrl{})
	beego.Router("/rest/products/:id:string", &rest.ProductCtrl{})

	beego.Router("/rest/cookie", &rest.CookieCtrl{}, "post:NewCookie")
	beego.Router("/rest/cookie/finish", &rest.CookieCtrl{}, "put:FinishCookie")
	beego.Router("/rest/cookie/buy", &rest.CookieCtrl{}, "put:BuyCookie")

}
