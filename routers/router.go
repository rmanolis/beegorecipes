package routers

import (
	"beegorecipes/controllers"
	"beegorecipes/utils"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"log"
)

func check(ctx *context.Context) {
	log.Println("AAAB")
}

func init() {
	gdt := utils.NewDataStore("beegorecipes",
		"127.0.0.1", "27017")
	beego.InsertFilter("/*", beego.AfterExec, check)
	beego.InsertFilter("/", beego.AfterExec, check)

	beego.InsertFilter("/*", beego.BeforeRouter, gdt.SetDB)
	beego.InsertFilter("/", beego.BeforeRouter, utils.CheckUserFilter)
	beego.InsertFilter("*", beego.BeforeRouter, utils.CheckUserFilter)

	beego.Router("/", &controllers.MainCtrl{})

	authRouter()

	adminRouter()

	restRouter()

	visitorRouter()

	userRouter()
}
