package routers

import (
	"beegorecipes/controllers/user"

	"github.com/astaxie/beego"
)

func userRouter() {

	beego.Router("/user/settings", &user.UserSettingsCtrl{}, "get:GetPageSettings")
	beego.Router("/user/password", &user.UserSettingsCtrl{}, "put:EditPassword")

}
