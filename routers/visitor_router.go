package routers

import (
	"beegorecipes/controllers/visitor"
	"github.com/astaxie/beego"
)

func visitorRouter() {

	beego.Router("/products", &visitor.ListProductsCtrl{})
	beego.Router("/products/:id:string", &visitor.ProductCtrl{})
	beego.Router("/products/search", &visitor.SearchProductsCtrl{})

	beego.Router("/recipes", &visitor.ListRecipesCtrl{})
	beego.Router("/recipes/:id:string", &visitor.RecipeCtrl{})
	beego.Router("/recipes/search", &visitor.SearchRecipesCtrl{})
	beego.Router("/choosen/recipes", &visitor.ChoosenRecipesCtrl{})

	beego.Router("/pdf", &visitor.PdfCtrl{})

}
