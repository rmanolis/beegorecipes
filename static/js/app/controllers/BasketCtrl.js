app.controller('BasketCtrl',
    ["$scope","$rootScope","RecipeSrv","ProductSrv", 
    function($scope, $rootScope, RecipeSrv, ProductSrv) {
      $scope.choosenProducts = [];
      $scope.choosenRecipes = [];
      function updateChoosenRecipes(){
        RecipeSrv.getChoosenRecipes().done(function(crs){
          $scope.choosenRecipes = crs;
          $scope.$apply();
        });
      }
      function updateChoosenProducts(){
        ProductSrv.getChoosenProducts().done(function(prods){
          $scope.choosenProducts = prods;
          $scope.$apply();
        });
      }
      updateChoosenRecipes();
      updateChoosenProducts();

      $scope.rejectRecipe = function(id){
        RecipeSrv.rejectRecipe(id).done(function(){
          console.log("rejected");        
          $rootScope.$emit("updateChoosenRecipes",id);
        });
      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).done(function(){
          $rootScope.$emit("updateChoosenProducts",id);
        });
      }

      $rootScope.$on("updateChoosenRecipes",function(e,id){
        updateChoosenRecipes();
      })

      $rootScope.$on("updateChoosenProducts",function(e,id){
        updateChoosenProducts();
      });



    }]);

