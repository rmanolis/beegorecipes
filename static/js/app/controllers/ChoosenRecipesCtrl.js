app.controller('ChoosenRecipesCtrl',
    ["$scope", "UserSrv","RecipeSrv","IngredientSrv","ProductSrv",
    "PDFSrv","CookieSrv",
    function($scope,UserSrv, RecipeSrv, IngredientSrv, ProductSrv, PDFSrv, CookieSrv) {
      $scope.recipes  =[];
      $scope.ingredients  = [];
      $scope.recipeProducts  = [];
      $scope.ingredientProducts  = [];
      $scope.choosenProducts  = [];
      $scope.isUser = false;
      UserSrv.isUser().done(function(){
        $scope.isUser=true;
        $scope.$apply();
      });

      function updateRecipes(){
        RecipeSrv.getChoosenRecipes().done(function(crs){
          console.log(crs);
          $scope.recipes=crs;
          $scope.$apply();
        });
        IngredientSrv.getIngredientsByChoosenRecipes().done(function(ings){
          console.log(ings);
          $scope.ingredients = ings;
          $scope.$apply();
        });
        ProductSrv.getProductsByChoosenRecipes().done(function(prods){
          console.log(prods);
          $scope.recipeProducts = prods;
          $scope.$apply();
        });
      }

      function updateProducts(){
        ProductSrv.getChoosenProducts().done(function(prods){
          $scope.choosenProducts=prods;
          $scope.$apply();
        });
      }

      $scope.showProductsByIngredient = function(id){
        ProductSrv.getProductsByIngredient(id).done(function(ings){
          console.log(ings);
          $scope.ingredientProducts = ings;
          $scope.$apply();
        });
      }

      $scope.rejectRecipe = function(id){
        RecipeSrv.rejectRecipe(id).done(function(){
          console.log("rejected");        
          updateRecipes();
        });
      }

      $scope.chooseProduct = function(id){
        console.log(id);
        ProductSrv.chooseProduct(id).done(function(){
          console.log("added");
          updateIngredientProducts(id, true);
          updateRecipeProducts(id,true);
          updateProducts();
        });

      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).done(function(){
          console.log("removed");
          updateIngredientProducts(id, false);
          updateRecipeProducts(id,false);
          updateProducts();
        });
      }

      $scope.rejectAllProducts = function(){
        ProductSrv.getChoosenProducts().done(function(crs){
          for(i=0; i < crs.length ; i++ ){
            ProductSrv.rejectProduct(crs[i].Id);
          }
          updateIngredientProducts(null, false);
          updateRecipeProducts(null,false);

          updateProducts();
        });
      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).done(function(){
          console.log("removed");
          updateIngredientProducts(id,false);
          updateRecipeProducts(id,false);
          updateProducts();
        });
      }

      $scope.createPDF = function(){
        PDFSrv.createPDF();
      }



      function updateIngredientProducts(id, b){
        $.each($scope.ingredientProducts, function (i,obj) {
          if(obj.Id == id){
            obj.IsChoosen = b;
          }else if(id == null){
            obj.IsChoosen = b;
          }
          $scope.$apply();
        });
      }

      function updateRecipeProducts(id,b){
        $.each($scope.recipeProducts, function (i,obj) {
          if(obj.Id == id){
            obj.IsChoosen = b;
          }else if(id == null){
            obj.IsChoosen = b;
          }
          $scope.$apply();
        });
      }


      updateRecipes();
      updateProducts();


      $scope.toFinish = function () {
        CookieSrv.finishCookie().done(function(){
          window.location.pathname = "/recipes/search";
        })
      }



    }]);

