app.controller('IndexCtrl',
    ["$scope","$rootScope", "RecipeSrv","ProductSrv", 
    function($scope, $rootScope, RecipeSrv,ProductSrv) {
      $scope.recipes = [];
      $scope.products = [];
      function updateRecipes(){
        RecipeSrv.getRecipes("").done(function(data){
          $scope.recipes = data;
          $scope.$apply();
        });
      }

      function updateProducts(){
        ProductSrv.getProducts("").done(function(data){
          $scope.products = data;
          $scope.$apply();
        });
      }

      updateRecipes();
      updateProducts();

      $scope.chooseRecipe=function(id){
        console.log(id);
        RecipeSrv.chooseRecipe(id).done(function(){
          $rootScope.$emit("updateChoosenRecipes",id);
        }).fail(function(){
          console.log("failed");
        })
      }

      $scope.rejectRecipe=function(id){
        console.log(id);
        RecipeSrv.rejectRecipe(id).done(function(){
          $rootScope.$emit("updateChoosenRecipes",id);
        })
      }

      $scope.chooseProduct = function(id){
        console.log(id);
        ProductSrv.chooseProduct(id).done(function(){
          $rootScope.$emit("updateChoosenProducts",id);
        });

      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).done(function(){
          $rootScope.$emit("updateChoosenProducts",id);
        });
      }



      $rootScope.$on("updateChoosenRecipes",function(e,id){
        console.log("hey");
        
          $.each($scope.recipes, function (i, obj) {
            if(obj.Id == id){
              if(obj.IsChoosen){
                obj.IsChoosen = false;
              }else{
                obj.IsChoosen = true;
              }
              $scope.$apply();
            }
          })
      })

      $rootScope.$on("updateChoosenProducts",function(e,id){
        $.each($scope.products, function (i,obj) {
          if(obj.Id == id){
            if(obj.IsChoosen){
              obj.IsChoosen = false;
            }else{
              obj.IsChoosen = true;
            }
            $scope.$apply();
          }
        })
      });
      
    }]);


