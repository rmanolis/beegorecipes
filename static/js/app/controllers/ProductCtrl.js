app.controller('ProductCtrl',
    ["$scope","$rootScope","ProductSrv", 
    function($scope,$rootScope,ProductSrv) {
     $scope.id= window.location.pathname.replace("/products/","");
    console.log($scope.id);
    $scope.product = {};
    function updateProduct(){
      ProductSrv.getProduct($scope.id).done(function(data){
        $scope.product = data;
        console.log(data);
        $scope.$apply();
      })
    }
    updateProduct();
    $scope.choose = function(){
      ProductSrv.chooseProduct($scope.id).done(function(){
        $rootScope.$emit("updateChoosenProducts",$scope.id);
      })
    }

    $scope.reject = function(){
       ProductSrv.rejectProduct($scope.id).done(function(){
        $rootScope.$emit("updateChoosenProducts",$scope.id);
       })
    }

    $rootScope.$on("updateChoosenProducts",function(){
      updateProduct();
    })

      
    }]);

