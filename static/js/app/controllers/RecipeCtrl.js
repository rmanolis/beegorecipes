app.controller('RecipeCtrl',
    ["$scope","$rootScope","RecipeSrv", 
    function($scope,$rootScope,RecipeSrv) {
     $scope.id= window.location.pathname.replace("/recipes/","");
    console.log($scope.id);
    $scope.recipe = {};
    function updateRecipe(){
      RecipeSrv.getRecipe($scope.id).done(function(data){
        $scope.recipe = data;
        console.log(data);
        $scope.$apply();
      })
    }
    updateRecipe();
    $scope.choose = function(){
     RecipeSrv.chooseRecipe($scope.id).done(function(){
        $rootScope.$emit("updateChoosenRecipes",$scope.id);
      })
    }

    $scope.reject = function(){
       RecipeSrv.rejectRecipe($scope.id).done(function(){
        $rootScope.$emit("updateChoosenRecipes",$scope.id);
       })
    }

    $rootScope.$on("updateChoosenRecipes",function(){
      updateRecipe();
    })

 
    }]);

