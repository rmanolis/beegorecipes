app.controller('SearchProductsCtrl',
    ["$scope","$rootScope","HelperSrv", "IngredientSrv", "TagSrv","ProductSrv","AutocompleteSrv",
    function($scope, $rootScope,HelperSrv,IngredientSrv, TagSrv,ProductSrv,AutocompleteSrv) {
      $scope.sizeList = 20;
    $scope.name = "";
    $scope.stags = "";
    $scope.singredients = "";
    $scope.products  = [];
    $scope.tags =[];
    $scope.ingredients =[]; 
     AutocompleteSrv.autocomplete("#tags",TagSrv.getTags, function(id,terms){
      
    });

    AutocompleteSrv.autocomplete("#ingredients",IngredientSrv.getIngredients, function(id,terms){
    });

    var createPagination = function(name,tags,ingredients ,lo){
      $('#pagination').bootpag({
        total: Math.ceil(lo.Total/lo.Size),
        page: lo.Page + 1, 
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
      }).on("page", function(event, num){
        console.log(num);
        ProductSrv.searchProducts(name,tags,ingredients,num,$scope.sizeList).done(function(lo){
          console.log(lo);
          $scope.products = lo.Objects;
          $scope.$apply();
        });
      });
    }

    $scope.submit = function() {
      var ingredients = HelperSrv.split($scope.singredients);
      var tags = HelperSrv.split($scope.stags);
      var name = $scope.name;
      console.log(name);
      ProductSrv.searchProducts(name,tags,ingredients,1,$scope.sizeList).done(function(lo){
        console.log(lo);
        $scope.products = lo.Objects;
        $scope.$apply();
        createPagination(name,tags,ingredients ,lo);
      }).fail(function(){
        console.log("fails to receive ");
      }); 
    }

    $scope.submit();
    $scope.choose=function(id){
      console.log(id);
      ProductSrv.chooseProduct(id).done(function(){
        console.log("added");
        $rootScope.$emit("updateChoosenProducts",id);
      })
    }

    $scope.reject=function(id){
      console.log(id);
      ProductSrv.rejectProduct(id).done(function(){
         $rootScope.$emit("updateChoosenProducts",id);
      })
    }
    
    $rootScope.$on("updateChoosenProducts",function(e,id){
        $.each($scope.products, function (i, obj) {
            if(obj.Id == id){
              if(obj.IsChoosen){
                obj.IsChoosen = false;
              }else{
                obj.IsChoosen = true;
              }
              $scope.$apply();
            }
          })
      })



    }]);

