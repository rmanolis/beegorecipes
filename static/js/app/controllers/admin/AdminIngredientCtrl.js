app.controller('AdminIngredientCtrl',
    ["$scope", "IngredientSrv","TagSrv","HelperSrv","AutocompleteSrv",
    function($scope,IngredientSrv,TagSrv,HelperSrv,AutocompleteSrv) {
      var pname = window.location.pathname;
      var id = pname.split('/').pop();
      console.log(id);

      $scope.tags =[];
      $scope.name = "";
      $scope.file = "";
      $scope.stags = "";

      IngredientSrv.getIngredient(id).done(function(ing){
        console.log(ing);
        $scope.name = ing.Name;
        $scope.tag = ing.Tags;
        $scope.stags = ing.STags; 
        $scope.$apply();
      });

      AutocompleteSrv.autocomplete("#tags",TagSrv.getTags, function(id, terms){
      });

      $scope.submit = function() {
        $scope.tags = HelperSrv.split($scope.stags);
        console.log($scope.name);
        if(id === "add"){
          IngredientSrv.addIngredient($scope.name, 
              $scope.tags,$scope.file).done(function(){
            console.log("done!");
            window.location.pathname = "/admin/ingredients";
          }).fail(function(){
                console.log("failed");
              });
        }else{
          IngredientSrv.editIngredient(id, $scope.name, 
              $scope.tags,$scope.file).done(function(){
            console.log("done!");
            window.location.pathname = "/admin/ingredients";
          }).fail(function(){
                console.log("failed");
              });

        }

      }

      $scope.cancel = function(){
        window.location.pathname = "/admin/ingredients";
      }


      $scope.fileUpload = function(e)
      {
        var file    = e.target.files[0];
        $scope.file = file;  
        $scope.$apply();
      };


    }]);

