app.controller('AdminListCookiesCtrl',
    ["$scope","CookieSrv", 
    function($scope,CookieSrv) {
      $scope.name = "";
      $scope.is_user = false;
      $scope.is_finished = false;
      $scope.cookies = [];
      $scope.sizeList = 20;

      $scope.createPagination = function(name,is_user,is_finished,lo){
        $('#pagination').bootpag({
          total: Math.ceil(lo.Total/lo.Size),
          page: lo.Page + 1, 
          firstLastUse: true,
          first: '←',
          last: '→',
          wrapClass: 'pagination',
          activeClass: 'active',
          disabledClass: 'disabled',
          nextClass: 'next',
          prevClass: 'prev',
          lastClass: 'last',
          firstClass: 'first'
        }).on("page", function(event, num){
          console.log(num);
          CookieSrv.searchCookies(name,is_user,is_finished,num,$scope.sizeList).done(function(lo){
            console.log(lo);
            $scope.cookies = lo.Objects;
            $scope.$apply();
          });
        });
      }
      $scope.submit = function() {
        var name = $scope.name;
        var is_user = $scope.is_user;
        var is_finished = $scope.is_finished;
        CookieSrv.searchCookies(name,is_user,is_finished,1,$scope.sizeList).done(function(lo){
          console.log(lo);
          $scope.cookies = lo.Objects;
          $scope.$apply();
          $scope.createPagination(name,is_user,is_finished,lo);
        }).fail(function(){
          console.log("fails to receive ");
        }); 
      }


    }]);

