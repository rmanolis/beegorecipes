app.controller('AdminListIngredientsCtrl',
    ["$scope", "AutocompleteSrv","IngredientSrv","TagSrv","HelperSrv",
    function($scope,AutocompleteSrv,IngredientSrv,TagSrv,HelperSrv) {
      $scope.sizeList = 20;
      $scope.name ="";
      $scope.stags = "";
      $scope.tags = [];
      $scope.ingredients = [];
      AutocompleteSrv.autocomplete("#tags",TagSrv.getTags, function(id,terms){
      });

      AutocompleteSrv.autocomplete("#ingredients",IngredientSrv.getIngredients, function(id,terms){
      });

      $scope.createPagination = function(name,tags,lo){
        $('#pagination').bootpag({
          total: Math.ceil(lo.Total/lo.Size),
          page: lo.Page + 1, 
          firstLastUse: true,
          first: '←',
          last: '→',
          wrapClass: 'pagination',
          activeClass: 'active',
          disabledClass: 'disabled',
          nextClass: 'next',
          prevClass: 'prev',
          lastClass: 'last',
          firstClass: 'first'
        }).on("page", function(event, num){
          console.log(num);
          IngredientSrv.searchIngredients(name,tags,num,$scope.sizeList).done(function(lo){
            console.log(lo);
            $scope.ingredients = lo.Objects;
          });
        });
      }

      $scope.submit = function() {
        var tags = HelperSrv.split($scope.stags);
        var name = $scope.name;
        IngredientSrv.searchIngredients(name,tags,1,$scope.sizeList).done(function(lo){
          console.log(lo);
          $scope.ingredients = lo.Objects;
          $scope.createPagination(name,tags,lo);
          $scope.$apply();
        }).fail(function(){
          console.log("fails to receive ");
        }); 
      }

      $scope.delete = function(id){
        IngredientSrv.deleteIngredient(id).done(function(){
          $scope.submit();
        });
      }

      $scope.submit();




    }]);

