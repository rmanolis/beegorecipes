app.controller('AdminListRecipesCtrl',
     ["$scope", "$rootScope","HelperSrv", "IngredientSrv", "TagSrv","RecipeSrv","AutocompleteSrv",
    function($scope, $rootScope,HelperSrv,IngredientSrv, TagSrv,RecipeSrv,AutocompleteSrv) {
    $scope.sizeList = 20;
    $scope.name = "";
    $scope.stags = "";
    $scope.singredients = "";
    $scope.recipes  = [];
    $scope.tags =[];
    $scope.ingredients =[]; 

    AutocompleteSrv.autocomplete("#tags",TagSrv.getTags, function(id,terms){
      
    });

    AutocompleteSrv.autocomplete("#ingredients",IngredientSrv.getIngredients, function(id,terms){
    });

    var createPagination = function(name,tags,ingredients ,lo){
      $('#pagination').bootpag({
        total: Math.ceil(lo.Total/lo.Size),
        page: lo.Page + 1, 
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
      }).on("page", function(event, num){
        console.log(num);
        RecipeSrv.searchRecipes(name,tags,ingredients,num,$scope.sizeList).done(function(lo){
          console.log(lo);
          $scope.recipes = lo.Objects;
          $scope.$apply();
        });
      });
    }

   $scope.submit = function() {
      var ingredients = HelperSrv.split($scope.singredients);
      var tags = HelperSrv.split($scope.stags);
      var name = $scope.name;
      console.log(name);
      RecipeSrv.searchRecipes(name,tags,ingredients,1,$scope.sizeList).done(function(lo){
        console.log(lo);
        $scope.recipes = lo.Objects;
        $scope.$apply();
        createPagination(name,tags,ingredients ,lo);
      }).fail(function(){
        console.log("fails to receive ");
      }); 
    }

    $scope.submit();

    $scope.delete = function(id){
      RecipeSrv.deleteRecipe(id).done(function(){
        $scope.submit();
      });
    }
 
  


    }]);

