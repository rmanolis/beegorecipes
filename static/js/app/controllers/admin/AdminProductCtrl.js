app.controller('AdminProductCtrl',
    ["$scope", "ProductSrv","AutocompleteSrv","HelperSrv","TagSrv","IngredientSrv",
    function($scope, ProductSrv,AutocompleteSrv,HelperSrv,TagSrv,IngredientSrv) {
      var pname = window.location.pathname;
      var id = pname.split('/').pop();
      console.log(id);

      $scope.name = "";
      $scope.productType ="ingredient";    
      $scope.description = "";
      $scope.price = 0.0;
      $scope.stags = "";
      $scope.tags = [];

      $scope.singredients = "";
      $scope.ingredients = [];
      $scope.file = "";

      ProductSrv.getProduct(id).done(function(prod){
        console.log(prod);
        $scope.name = prod.Name;
        $scope.productType = prod.ProductType;
        $scope.description = prod.Description;
        $scope.price = prod.Price;
        $scope.ingredients = prod.Ingredients;
        $scope.tags = prod.Tags;

        $scope.stags = HelperSrv.createStringFromList(prod.STags);
        $scope.singredients = HelperSrv.createStringFromList(prod.SIngredients);
        $scope.$apply();
      });

      AutocompleteSrv.autocomplete("#tags",TagSrv.getTags, function(data){
        //self.tags.push(data);
      });

      AutocompleteSrv.autocomplete("#ingredients",IngredientSrv.getIngredients, function(data){
        //self.ingredients.push(data);
      });


      $scope.submit = function() {
        $scope.tags = HelperSrv.split($scope.stags);
        $scope.ingredients = HelperSrv.split($scope.singredients);
        if(id === "add"){
          ProductSrv.addProduct($scope.name, 
              $scope.productType,
              $scope.description,
              $scope.price,
              $scope.tags,
              $scope.ingredients,$scope.file).done(function(){
            console.log("done!");
            window.location.pathname = "/admin/products";
          }).fail(function(){
                console.log("failed");
              });
        }else{
          ProductSrv.editProduct(id,$scope.name, 
              $scope.productType,
              $scope.description,
              $scope.price,
              $scope.tags,
              $scope.ingredients,$scope.file).done(function(){
            console.log("done!");
            window.location.pathname = "/admin/products";
          }).fail(function(){
                console.log("failed");
              });

        }
      }

      $scope.fileUpload = function( e)
    {
        var file    = e.target.files[0];
        $scope.file = file;    
    }



      }])

