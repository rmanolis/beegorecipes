app.controller('AdminRecipeCtrl',
    ["$scope", "RecipeSrv","IngredientSrv","ProductSrv","TagSrv","HelperSrv","AutocompleteSrv",
    function($scope,RecipeSrv, IngredientSrv, ProductSrv,TagSrv,HelperSrv,AutocompleteSrv) {
      var pname = window.location.pathname;
    var id = pname.split('/').pop();
    console.log(id);

    $scope.name = "";
    $scope.steps = "";
    $scope.servings = "";
    
    $scope.stags = "";
    $scope.tags = [];
    
    $scope.singredients = "";
    $scope.ingredients = [];

    $scope.sproducts = "";
    $scope.products = [];
    $scope.file = "";
    
    
    RecipeSrv.getRecipe(id).done(function(rec){
      console.log(rec);
      $scope.name = rec.Name;
      $scope.steps = rec.Steps;
      $scope.servings = rec.Servings;
      $scope.ingredients = rec.Ingredients;
      $scope.tags = rec.Tags;
      $scope.products = rec.Products ;
      
      $scope.stags = HelperSrv.createStringFromList(rec.STags);
      
      $scope.singredients = HelperSrv.createStringFromList(rec.SIngredients);
      
      $scope.sproducts = HelperSrv.createStringFromList(rec.SProducts);
      $scope.$apply();

    });

    AutocompleteSrv.autocomplete("#tags",TagSrv.getTags, function(data){
      //self.tags.push(data);
    });

    AutocompleteSrv.autocomplete("#ingredients",IngredientSrv.getIngredients, function(data){
      //self.ingredients.push(data);
    });

    AutocompleteSrv.autocomplete("#products",ProductSrv.getProducts, function(data){
    });



    $scope.submit = function() {
      $scope.tags = HelperSrv.split($scope.stags);
      $scope.ingredients = HelperSrv.split($scope.singredients);
      $scope.products = HelperSrv.split($scope.sproducts);
      console.log($scope.products);
      if(id === "add"){
        RecipeSrv.addRecipe($scope.name, 
            $scope.servings,
            $scope.steps,
            $scope.tags, $scope.ingredients,
            $scope.products, $scope.file).done(function(){
          console.log("done!");
          window.location.pathname = "/admin/recipes";
        }).fail(function(){
          console.log("failed");
        });
      }else{
        RecipeSrv.editRecipe(id,$scope.name, 
            $scope.servings,
            $scope.steps,
            $scope.tags,$scope.ingredients,$scope.products, $scope.file).done(function(){
          console.log("done!");
          window.location.pathname = "/admin/recipes";
        }).fail(function(){
          console.log("failed");
        });

      }
      
    }
    
    $scope.fileUpload = function( e)
    {
        var file    = e.target.files[0];
        $scope.file = file;    
    }

    $scope.cancel = function(){
      window.location.pathname = "/admin/recipes";
    }


    }]);

