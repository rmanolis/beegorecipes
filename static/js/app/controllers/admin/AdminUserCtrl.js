app.controller('AdminUserCtrl',
    ["$scope","CookieSrv", 
    function($scope,CookieSrv) {

      $scope.cookies = [];
      $scope.ingredients = [];
      $scope.products = [];
      $scope.recipeProducts = [];
      $scope.createPagination = function(id ,lo){
        $('#pagination').bootpag({
          total: Math.ceil(lo.Total/lo.Size),
          page: lo.Page + 1, 
          firstLastUse: true,
          first: '←',
          last: '→',
          wrapClass: 'pagination',
          activeClass: 'active',
          disabledClass: 'disabled',
          nextClass: 'next',
          prevClass: 'prev',
          lastClass: 'last',
          firstClass: 'first'
        }).on("page", function(event, num){
          console.log(num);
          CookieSrv.getUserCookies(id).done(function(lo){
            $scope.cookies = lo.Objects;
            $scope.$apply();
            $scope.createPagination(id, lo);
          });
        });
      }
      var uid =  window.location.pathname.replace("/admin/users/", "")
        CookieSrv.getUserCookies(uid, 1, 20).done(function(lo){
          console.log(lo);
          $scope.cookies=lo.Objects;
          $scope.$apply();
          $scope.createPagination(uid,lo);
        })
      $scope.chooseCookie = function(id){
        console.log(id);
        CookieSrv.getUserProducts(uid, id).done(function(products){
          $scope.products = products;
          $scope.$apply();
        });
        CookieSrv.getUserIngredients(uid, id).done(function(ings){
          $scope.ingredients = ings;
          $scope.$apply();
        });
        CookieSrv.getUserRecipesProducts(uid, id).done(function(products){
          $scope.recipeProducts = products;
          $scope.$apply();
        });
      }




    }]);

