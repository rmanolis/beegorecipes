app.factory('AutocompleteSrv',
    ["$http","HelperSrv",
    function($http,HelperSrv){
  var obj = {};

    obj.autocomplete = function(css, serviceCb, outputCb){
    $(css)
      // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          console.log(request.term);
          serviceCb(HelperSrv.extractLast( request.term )).done(function(data){
            console.log(data);
            response($.map(data, function (item) {
                       return { value: item.Id, label: item.Name };
                    }));  
          });
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = HelperSrv.split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.label );
          outputCb(ui.item.value, terms);
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });

  }

  return obj;
    }])

