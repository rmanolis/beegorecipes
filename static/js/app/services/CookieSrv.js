app.factory('CookieSrv',
    [
    function(){
  var obj = {};
  obj.finishCookie = function ( ) {
    return $.ajax({
      method: "PUT",
      url: "/rest/cookie/finish",
    });
  }

  obj.buyCookie = function ( ) {
    return $.ajax({
      method: "PUT",
      url: "/rest/cookie/buy",
    });
  }


  obj.newCookie = function(){
    return $.ajax({
      method: "POST",
      url: "/rest/cookie",
    });

  }

  obj.searchCookies = function(name,is_user,is_finished,page,size){
    return $.ajax({
      method: "POST",
      url: "/admin/search/cookies?p="+ page + "&s="+
        size ,
      data:{
        Name:name,
        IsUser:is_user,
        IsFinished:is_finished,
      }
     });
  }

  obj.getUserCookies = function(id,page,size){
    return $.ajax({
      method: "GET",
      url: "/admin/users/"+id+"/cookies?p="+ page + "&s="+
        size,
    });
  }

  obj.getUserProducts = function(user_id, cookie_id){
    return $.ajax({
      method: "POST",
      url: "/admin/users/"+user_id+"/products",
      data:{
        CookieId:cookie_id
      }
    });
  }

  obj.getUserIngredients = function(user_id,cookie_id){
    return $.ajax({
      method: "POST",
      url: "/admin/users/"+user_id+"/ingredients",
      data:{
        CookieId:cookie_id
      }
    });
  }

  obj.getUserRecipesProducts = function(user_id,cookie_id){
    return $.ajax({
      method: "POST",
      url: "/admin/users/"+user_id+"/recipes/products",
      data:{
        CookieId:cookie_id
      }

    });
  }

  return obj; 
    }])
