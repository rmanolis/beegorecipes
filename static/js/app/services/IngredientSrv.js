app.factory('IngredientSrv',
    [
    function(){
  var obj = {};
  
  obj.getIngredients = function(name){
    return $.ajax({
      method: "GET",
      url: "/rest/ingredients?" + "Name="+name,
    });
  }

  obj.addIngredient = function(name, tags,file){
    var fd = new FormData();
    fd.append("file", file);
    fd.append("Name", name);
    fd.append("Tags", JSON.stringify(tags));
    return $.ajax({
      method: "POST",
      url: "/admin/ingredients/add",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    })
  }

  obj.editIngredient = function(id, name, tags, file){
    var fd = new FormData();
    fd.append("file", file);
    fd.append("Name", name);
    fd.append("Tags", JSON.stringify(tags));
    return $.ajax({
      method: "PUT",
      url: "/admin/ingredients/"+id,
      cache: false,
      contentType: false,
      processData: false,
      data:fd,    }) 
  }

  obj.deleteIngredient = function(id){
    return $.ajax({
      method: "DELETE",
      url: "/admin/ingredients/"+id,
      }) 
  }


  obj.searchIngredients = function(name, tags, page, size){
    return $.ajax({
      method: "POST",
      url: "/rest/search/ingredients?p="+ page + "&s="+size,
      data: {
        "Name": name ,
        "Tags": tags,
      }
    })

  }


  obj.getIngredient = function(id){
    return $.ajax({
      method: "GET",
      url: "/rest/ingredients/"+id
    }) 
  }

  obj.getIngredientsByChoosenRecipes = function(){
    return $.ajax({
      method: "GET",
      url: "/rest/choosen/recipes/ingredients",
    })
  }

  obj.getIngredientsByRecipe = function(id){
    return $.ajax({
      method: "GET",
      url: "/rest/recipes/"+id+"/ingredients",
    })
  }


  return obj;
    }]);
