app.factory('PDFSrv',
    [
    function(){
      var obj = {};
      var margins = {
        top: 10,
        bottom: 60,
        left: 40,
        width: 522
      };


      obj.createPDF = function(){
        var pdf = new jsPDF('p','pt','a5');
        $.ajax({
          method: "GET",
          url: "/pdf",
        }).success(function(data){
          document.getElementById("pdf").innerHTML = data;
          pdf.addHTML(
              document.getElementById("pdf"),
              function(dispose) {
                pdf.save('buy_list.pdf');
                document.getElementById("pdf").innerHTML = "";
              });
        });
      }

      return obj;
    }]);
