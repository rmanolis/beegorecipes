app.factory('ProductSrv',
    [
    function(){
  var obj = {};

obj.getProducts = function(name){
    return $.ajax({
      method: "GET",
      url: "/rest/products?" + "Name="+name,
    });
  }

  obj.getProduct = function(id){
    return $.ajax({
      method: "GET",
      url: "/rest/products/"+id
    }) 
  }

  obj.addProduct = function(name, productType, description,
      price, tags, ingredients,file){
    var fd = new FormData();
    fd.append("Name", name);
    fd.append("ProductType",productType);
    fd.append("Description",description);
    fd.append("Price",price);
    fd.append("Tags", JSON.stringify(tags));
    fd.append("Ingredients", JSON.stringify(ingredients));
    fd.append("file", file);
    return $.ajax({
      method: "POST",
      url: "/admin/products/add",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    })
  }

  obj.editProduct = function(id, name,productType, description,
      price, tags, ingredients,file){
    var fd = new FormData();
    fd.append("Name", name);
    fd.append("ProductType",productType);
    fd.append("Description",description);
    fd.append("Price",price);
    fd.append("Tags", JSON.stringify(tags));
    fd.append("Ingredients", JSON.stringify(ingredients));
    fd.append("file", file);
    return $.ajax({
      method: "PUT",
      url: "/admin/products/"+id,
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    }); 
  }

  obj.deleteProduct = function(id){
    return $.ajax({
      method: "DELETE",
      url: "/admin/products/"+id,
      });
  }

  
   obj.searchProducts = function(name, tags, ingredients, page, size){
    return $.ajax({
      method: "POST",
      url: "/rest/search/products?p="+ page + "&s="+size,
      data: {
        "Name": name ,
        "Tags": tags,
        "Ingredients": ingredients,
      }
    })

  }


  obj.getProductsByChoosenRecipes = function(){
    return $.ajax({
      method: "GET",
      url: "/rest/choosen/recipes/products",
    })
  }

  obj.getProductsByIngredient = function(id){
    return $.ajax({
      method: "GET",
      url: "/rest/ingredients/"+ id +"/products",
    })
  }

  obj.getChoosenProducts =  function(){
    return $.ajax({
      method: "GET",
      url: "/rest/choosen/products",
    })

  }

  obj.chooseProduct = function(id){
    return $.ajax({
      method: "POST",
      url: "/rest/choosen/products/" + id,
    })
  }

  obj.rejectProduct = function(id){
    return $.ajax({
      method: "DELETE",
      url: "/rest/choosen/products/" + id ,
    })
  }



  return obj;
    }]);
