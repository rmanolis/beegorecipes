app.factory('RecipeSrv',
    [
    function(){
  var obj = {};
   obj.getRecipes = function(name){
    return $.ajax({
      method: "GET",
      url: "/rest/recipes?" + "Name="+name,
    });
  }

  obj.getRecipe = function(id){
    return $.ajax({
      method: "GET",
      url: "/rest/recipes/"+id
    }) 
  }

  obj.addRecipe = function(name, servings, steps, tags, 
      ingredients, products,file){
    var fd = new FormData();
    fd.append("Name", name);
    fd.append("Servings", servings);
    fd.append("Steps", steps);
    fd.append("Tags", JSON.stringify(tags));
    fd.append("Ingredients", JSON.stringify(ingredients));
    fd.append("Products", JSON.stringify(products));
    fd.append("file", file);

    return $.ajax({
      method: "POST",
      url: "/admin/recipes/add",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    })
  }

  obj.editRecipe = function(id, name, servings, steps, 
      tags, ingredients, products,file){
    var fd = new FormData();
    fd.append("Name", name);
    fd.append("Servings", servings);
    fd.append("Steps", steps);
    fd.append("Tags", JSON.stringify(tags));
    fd.append("Ingredients", JSON.stringify(ingredients));
    fd.append("Products", JSON.stringify(products));
    fd.append("file", file);

    return $.ajax({
      method: "PUT",
      url: "/admin/recipes/"+id,
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    }) 
  }

  obj.deleteRecipe = function(id){
    return $.ajax({
      method: "DELETE",
      url: "/admin/recipes/"+id,
      }) 
  }

  obj.searchRecipes = function(name, tags, ingredients, page, size){
    return $.ajax({
      method: "POST",
      url: "/rest/search/recipes?p="+ page + "&s="+size,
      data: {
        "Name": name ,
        "Tags": tags,
        "Ingredients": ingredients,
      }
    })

  }

  obj.getChoosenRecipes =  function(){
    return $.ajax({
      method: "GET",
      url: "/rest/choosen/recipes",
    })

  }
  obj.chooseRecipe = function(id){
    return $.ajax({
      method: "POST",
      url: "/rest/choosen/recipes/" + id,
    })
  }

  obj.rejectRecipe = function(id){
    return $.ajax({
      method: "DELETE",
      url: "/rest/choosen/recipes/" + id ,
    })
  }

  return obj;
    }]);
