app.factory('UserSrv',
    [
    function(){
  var obj = {};
 obj.searchUsers = function(name,email,page,size){
    return $.ajax({
      method: "POST",
      url: "/admin/search/users?p="+ page + "&s="+
        size ,
      data:{
        Name:name,
        Email:email
      }
    });
  }

  obj.isUser = function(){
     return $.ajax({
      method: "GET",
      url: "/auth/user",    
     });

  }

  obj.editPassword = function(pass, repass){
     return $.ajax({
      method: "PUT",
      url: "/user/password",  
      data: {
        Password:pass,
        RepeatPassword:repass,
      },
     });

  }

 
  return obj;
    }]);
