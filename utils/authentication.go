package utils

import (
	"github.com/astaxie/beego/context"
	"log"
	"math/rand"
	"time"
)

var SECRET = "secret"

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandSeq(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func CheckUserFilter(ctx *context.Context) {
	log.Println("check user")
	_, ok := ctx.Input.Session("admin_id").(string)
	if ok {
		ctx.Input.Data["IsAdmin"] = true
	}
	_, ok = ctx.Input.Session("user_id").(string)
	if ok {
		ctx.Input.Data["IsUser"] = true
	}

}

func GetCookie(ctx *context.Context) string {
	str, _ := ctx.GetSecureCookie(SECRET, "cookie_id")
	return str
}

func AdminOnlyFilter(ctx *context.Context) {
	log.Println("Call IsAdminFilter")
	_, ok := ctx.Input.Session("admin_id").(string)
	if !ok {
		ctx.Redirect(302, "/auth/login")
	}
	ctx.Input.Data["IsAdmin"] = true
}

func UserOnlyFilter(ctx *context.Context) {
	log.Println("Call IsUserFilter")
	_, ok := ctx.Input.Session("user_id").(string)
	if !ok {
		ctx.Redirect(302, "/auth/login")
	}
}

func NotUserOnlyFilter(ctx *context.Context) {
	log.Println("Call IsNotUserFilter")
	_, ok := ctx.Input.Session("user_id").(string)
	if ok {
		ctx.Redirect(302, "/")
	}
}
