package chelpers

import (
	"beegorecipes/models"
	"net/http"

	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func CheckUser(db *mgo.Database, c beego.Controller) (*models.User, int) {
	id, ok := c.GetSession("user_id").(string)
	if !ok {
		return nil, http.StatusUnauthorized
	}

	if !bson.IsObjectIdHex(id) {
		return nil, http.StatusUnauthorized
	}
	uid := bson.ObjectIdHex(id)
	user, err := models.GetUserById(db, uid)
	if err != nil {
		return nil, http.StatusUnauthorized
	}
	return user, 0
}
