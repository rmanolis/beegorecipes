package utils

import (
	"errors"
	"io/ioutil"
	"log"
	"mime/multipart"

	"github.com/rakyll/magicmime"
)

func ReadFile(header *multipart.FileHeader) ([]byte, error) {
	err := magicmime.Open(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_SYMLINK | magicmime.MAGIC_ERROR)
	defer magicmime.Close()
	if err != nil {
		panic(err)
	}

	file, err := header.Open()
	if err != nil {
		return nil, err
	}
	buf, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	mt, err := magicmime.TypeByBuffer(buf)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	if mt != "image/jpeg" && mt != "image/png" {
		return nil, errors.New("it is not the correct type")
	}

	return buf, nil
}

func CreateFile(content []byte, filename string) error {
	return ioutil.WriteFile("static/images/"+filename, content, 0755)
}
