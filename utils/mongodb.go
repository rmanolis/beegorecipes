package utils

import (
	"fmt"
	"github.com/astaxie/beego/context"
	"gopkg.in/mgo.v2"
)

type DataStore struct {
	DBName  string
	Session *mgo.Session
}

func NewDataStore(name, ip, port string) *DataStore {
	serv := fmt.Sprint(ip, ":", port)
	fmt.Println(serv)
	session, err := mgo.Dial(serv)
	if err != nil {
		panic(err)
	}
	dt := new(DataStore)
	dt.Session = session
	dt.DBName = name

	return dt
}

func GetDB(ctx *context.Context) (*mgo.Database, *mgo.Session) {
	db := ctx.Input.Session("db").(*mgo.Database)
	ses := ctx.Input.Session("dbsession").(*mgo.Session)
	return db, ses
}

func (dt *DataStore) SetDB(ctx *context.Context) {
	reqSession := dt.Session.Clone()
	db := reqSession.DB(dt.DBName)
	ctx.Output.Session("db", db)
	ctx.Output.Session("dbsession", reqSession)
}

func (dt *DataStore) RemoveDB(ctx *context.Context) {
	fmt.Println("RemoveDB")
	reqSession := ctx.Input.Session("dbsession").(mgo.Session)
	reqSession.Close()
	fmt.Println("RemoveDB")
}
