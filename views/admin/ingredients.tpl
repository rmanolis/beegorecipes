{% template "base.tpl" . %}

{% define "content" %}

<form action="/admin/ingredients/add">
    <input type="submit" value="Add ingredient">
</form>

<div ng-controller="AdminListIngredientsCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>


<button ng-click="submit()">Submit</button>
</div>

<div class="ui-widget">
  <ul ng-repeat="ingredient in ingredients">
    <li class="ingredient">
        <a href="{{'/admin/ingredients/'+ingredient.Id }}" > <strong >{{ingredient.Name}}</strong></a>
        <button ng-click="delete(ingredient.Id)">Delete</button>
    </li>
  </ul>
</div>


<div id="pagination"></div>
</div>



{% end %}
