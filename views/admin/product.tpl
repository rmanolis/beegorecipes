{% template "base.tpl" . %}

{% define "content" %}

 <div ng-controller="AdminProductCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Product type:  <select ng-model="productType">
<option value="ingredient">Ingredient</option>
<option value="desert">Desert</option>
<option value="drink">Drink</option>  
<option value="side_dish">Side dish</option>
<option value="tool">Tool</option>
</select> 
<br/>

Price: <input ng-model="price" type="number">
<br/>

Tags: <input id="tags" size="50"  ng-model="stags">
<br/>
Ingredients: <input id="ingredients" size="50"  ng-model="singredients">
<br/>
<input custom-on-change="fileUpload" type="file" accept="image/jpeg" class="fileChooser"/>

<br/>

Description: 
<ng-quill-editor ng-model="description" toolbar="true" link-tooltip="true" image-tooltip="true" toolbar-entries="font size bold list bullet italic underline strike align color background link image" editor-required="true" required="" error-class="input-error"></ng-quill-editor>
  <br/>

  <button ng-click="submit()">Submit</button>
  <button ng-click="cancel()">Cancel</button>

  </div>
</div>
{% end %}
