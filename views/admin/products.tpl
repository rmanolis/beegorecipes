{% template "base.tpl" . %}

{% define "content" %}


<div ng-controller="AdminListProductsCtrl">
<form action="/admin/products/add">
    <input type="submit" value="Add product">
</form>

<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>
Ingredients: <input id="ingredients" ng-model="singredients" size="50" >
<br/>

<button  ng-click="submit()">Submit</button>

</div>

<div class="ui-widget">
  <ul ng-repeat="product in products">
    <li class="product">
        <a href="{{'/admin/products/'+product.Id}}" > <strong >{{product.Name}}</strong></a>
        <button ng-click="delete(product.Id)">Delete</button>
    </li>
  </ul>
</div>


<div id="pagination"></div>

</div>


{% end %}
