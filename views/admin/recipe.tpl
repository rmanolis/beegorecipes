{% template "base.tpl" . %}

{% define "content" %}
<div ng-controller="AdminRecipeCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>
Ingredients: <input id="ingredients" ng-model="singredients" size="50" >
<br/>
Products: <input id="products" size="50"  ng-models="sproducts">
<br/>
<input custom-on-change="fileUpload" type="file" accept="image/jpeg" class="fileChooser"/>

<br/>
Servings: 
<ng-quill-editor ng-model="servings" toolbar="true" link-tooltip="true" image-tooltip="true" toolbar-entries="font size bold list bullet italic underline strike align color background link image" editor-required="true" required="" error-class="input-error"></ng-quill-editor>

<br/>

Execution: 
<ng-quill-editor ng-model="execution" toolbar="true" link-tooltip="true" image-tooltip="true" toolbar-entries="font size bold list bullet italic underline strike align color background link image" editor-required="true" required="" error-class="input-error"></ng-quill-editor>

<br/>

<button ng-click="submit()">Submit</button>
<button ng-click="cancel()">Cancel</button>

</div>
</div>

{% end %}
