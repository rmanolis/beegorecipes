{% template "base.tpl" . %}

{% define "content" %}

<form action="/admin/tags/add">
    <input type="submit" value="Add Tag">
</form>

<ul>
{%range $key, $val := .tags%}
   <li>{%$val.Name%} 
    <form action="/admin/tags/{%$val.Id.Hex%}">
      <input type="submit" value="Edit">
    </form>
    <form action="/admin/tags/{%$val.Id.Hex%}" method="POST">
      <input type="hidden" name="_method" value="DELETE">
      <input type="submit" value="delete">
    </form>
   </li>
{%end%}
</ul>


{%if gt .paginator.PageNums 1%}
<ul class="pagination pagination-sm">
    {%if .paginator.HasPrev%}
        <li><a href="{%.paginator.PageLinkFirst%}"></a></li>
        <li><a href="{%.paginator.PageLinkPrev%}">&lt;</a></li>
    {%else%}
        <li class="disabled"><a></a></li>
        <li class="disabled"><a>&lt;</a></li>
    {%end%}
    {%range $index, $page := .paginator.Pages%}
        <li{%if $.paginator.IsActive .%} class="active"{%end%}>
            <a href="{%$.paginator.PageLink $page%}">{%$page%}</a>
        </li>
    {%end%}
    {%if .paginator.HasNext%}
        <li><a href="{%.paginator.PageLinkNext%}">&gt;</a></li>
        <li><a href="{%.paginator.PageLinkLast%}"></a></li>
    {%else%}
        <li class="disabled"><a>&gt;</a></li>
        <li class="disabled"><a></a></li>
    {%end%}
</ul>
{%end%}


{% end %}
