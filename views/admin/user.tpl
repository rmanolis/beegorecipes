{% template "base.tpl" . %}

{% define "content" %}

<div ng-controller="AdminUserCtrl">

Cookies
<div class="ui-widget">
  <ul ng-repeat="cookie in cookies">
    <li class="cookie">
         <strong >{{cookie.Date}}</strong>
        <button ng-click="chooseCookie(cookie.Id)">Choose</button>
    </li>
  </ul>
</div>

<div id="pagination"></div>

Ingredients
<div class="ui-widget">
  <ul  ng-repeat="ingredient in ingredients">
    <li class="ingredient">
        <strong >{{ingredient.Name}}</strong>
    </li>
  </ul>
</div>

Products
<div class="ui-widget">
  <ul  ng-repeat="product in products">
    <li class="product">
         <a href="{{'/products/'+product.Id }}" ><strong>{{product.Name}}</strong></a>
    </li>
  </ul>
</div>

Recipe products
<div class="ui-widget">
  <ul ng-repeat="product in recipeProducts">
    <li class="product">
         <a href="{{ '/products/'+product.Id }}" ><strong >{{product.Name}}</strong></a>
    </li>
  </ul>
</div>

</div>




{% end %}
