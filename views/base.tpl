<!DOCTYPE html>
<html>
<head>
<title>Go Recipes! Go!</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/static/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css">
<link rel="stylesheet" href="/static/bower_components/quill/dist/quill.snow.css">

<script type="text/javascript" src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/static/bower_components/lodash/lodash.min.js"></script>    
<script type="text/javascript" src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="/static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="/static/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/static/bower_components/quill/dist/quill.min.js"></script>
<script type="text/javascript" src="/static/bower_components/bootpag/lib/jquery.bootpag.min.js"></script>
<script type="text/javascript" src="/static/bower_components/html2canvas/build/html2canvas.min.js"></script>
<script type="text/javascript" src="/static/bower_components/jspdf/dist/jspdf.min.js"></script>
<script type="text/javascript" src="/static/bower_components/ngquill/src/ng-quill.min.js"></script>

<script  type="text/javascript" src="/static/js/app/main.js"></script> 

<script  type="text/javascript" src="/static/js/app/services/CookieSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/HelperSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/IngredientSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/PDFSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/ProductSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/RecipeSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/TagSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/UserSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/AutocompleteSrv.js"></script> 

<script  type="text/javascript" src="/static/js/app/directives/CustomOnChangeDrv.js"></script> 

<script  type="text/javascript" src="/static/js/app/controllers/IndexCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/BasketCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/ChoosenRecipesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/ProductCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/RecipeCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/SearchProductsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/SearchRecipesCtrl.js"></script> 

<script  type="text/javascript" src="/static/js/app/controllers/user/UserSettingsCtrl.js"></script> 

<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListIngredientsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminIngredientCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListRecipesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminRecipeCtrl.js"></script>
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListProductsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminProductCtrl.js"></script>
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListCookiesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListUsersCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminUserCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminStatisticsCtrl.js"></script>


</head>
<body  ng-app="app">
<a href="/">Home</a>
<a href="/recipes/search">Recipes</a>
<a href="/products/search">Products</a>

<a href="/choosen/recipes">Choosen Recipes</a>
{% if .IsUser %}
<a href="/user/settings">Settings</a>
<a href="/auth/logout">Logout</a>
{% else %}
<a href="/auth/login">Login</a>
<a href="/auth/register">Register</a>
{% end %}
<br/>
{% if .IsAdmin %}
<a href="/admin/tags">Tags</a>    
<a href="/admin/ingredients">Ingredients</a> 
<a href="/admin/recipes">Recipes</a> 
<a href="/admin/products">Products</a> 
<a href="/admin/users">Users</a> 
<a href="/admin/cookies">Cookies</a> 
<a href="/admin/statistics">Statistics</a>
{% end %}


<br/>
{% .flash.error %}
{% .flash.notice %} 
{% .flash.success %}
<br/>

<div class="container">
{% template "content" . %}
</div>

</body>

</html>
