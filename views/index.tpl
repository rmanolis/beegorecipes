{% template "base.tpl" . %}

{% define "content" %}
<div ng-controller="IndexCtrl">
<h2>Go Recipes</h2>

List latest recipes
<div class="ui-widget">
  <ul ng-repeat="recipe in recipes">
    <li class="recipe">
        <a href="{{'/recipes/'+recipe.Id }}" > <strong>{{recipe.Name}}</strong></a>
         <div ng-if="!recipe.IsChoosen">
          <button ng-click="chooseRecipe(recipe.Id)">Choose</button>
        </div>
        <div ng-if="recipe.IsChoosen">
          <button  ng-click="rejectRecipe(recipe.Id)">Reject</button>
        </div>
    </li>
  </ul>
</div>

List latest products
<div class="ui-widget">
  <ul ng-repeat="product in products">
    <li class="product">
        <a href="{{'/products/'+product.Id }}" > <strong>{{product.Name}}</strong></a>
         <div ng-if="!product.IsChoosen">
          <button ng-click="chooseProduct(product.Id)">Choose</button>
        </div>
        <div ng-if="product.IsChoosen">
          <button  ng-click="rejectProduct(product.Id)">Reject</button>
        </div>
    </li>
  </ul>
</div>

</div>

{% end %}

{% template "visitor/basket.tpl" . %}

<div class="basket">
{% define "basket" %}
{% end %}
</div>


