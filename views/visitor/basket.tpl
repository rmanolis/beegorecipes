
<div ng-controller="BasketCtrl">
  <div class="ui-widget">
    Recipes
    <ul ng-repeat="recipe in choosenRecipes">
      <li class="recipe">
          <a href="{{'/recipes/'+ recipe.Id }}" > <strong >{{recipe.Name}}</strong></a>
            <button ng-click="rejectRecipe(recipe.Id)">Reject</button>
      </li>
    </ul>
  </div>
    <div class="ui-widget">
    Products
    <ul ng-repeat="product in choosenProducts">
      <li class="product">
          <a href="{{'/products/'+ product.Id }}" > <strong>{{product.Name}}</strong></a>
            <button  ng-click="rejectProduct(product.Id)">Reject</button>
      </li>
    </ul>
  </div>

</div>

