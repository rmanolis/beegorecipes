{% template "base.tpl" . %}

{% define "content" %}

<div ng-controller="ChoosenRecipesCtrl">
List Choosen Recipes
<div class="ui-widget">
  <ul ng-repeat="recipe in recipes">
      <li class="recipe">
          <a href="{{'/recipes/'+ recipe.Id }}" > <strong >{{recipe.Name}}</strong></a>
            <button ng-click="rejectRecipe(recipe.Id)">Reject</button>
      </li>
    </ul>
</div>
</br>
Show products by recipes
<div class="ui-widget">
  <ul ng-repeat="product in recipeProducts">
    <li class="product">
        <a href="{{'/products/'+ product.Id }}" > <strong>{{product.Name}}</strong></a>
        <div ng-if="!product.IsChoosen">
          <button ng-click="chooseProduct(product.Id)">Choose</button>
        </div>
        <div ng-if="product.IsChoosen">
          <button  ng-click="rejectProduct(product.Id)">Reject</button>
        </div>
    </li>
  </ul>
</div>

List Ingredients
<div class="ui-widget">
  <ul ng-repeat="ingredient in ingredients">
    <li class="ingredient">
        <strong>{{ingredient.Name}}</strong>
        <button ng-click="showProductsByIngredient(ingredient.Id)">Show</button>
    </li>
  </ul>
</div>

<br/>


Show products by choosen ingredient and check 
<div class="ui-widget">
  <ul ng-repeat="product in ingredientProducts">
     <li class="product">
        <a href="{{'/products/'+ product.Id }}" > <strong>{{product.Name}}</strong></a>
        <div ng-if="!product.IsChoosen">
          <button ng-click="chooseProduct(product.Id)">Choose</button>
        </div>
        <div ng-if="product.IsChoosen">
          <button  ng-click="rejectProduct(product.Id)">Reject</button>
        </div>
    </li>
  </ul>
</div>

<br/>

List choosen Product
<button ng-click="rejectAllProducts()">Reject All</button>

<div class="ui-widget">
  <ul ng-repeat="product in  choosenProducts">
    <li class="product">
        <a href="{{'/products/'+ product.Id }}" > <strong>{{product.Name}}</strong></a>
        <button  ng-click="rejectProduct(product.Id)">Reject</button>
    </li>
  </ul>
</div>
<br/>


<button ng-click="createPDF()">Create PDF</button>
<button ng-click="toFinish()">Τελείωσα</button>

<br/>


</div>
<div style="background:#fff" id="pdf">
</div>


{%end%}
