Ingredients
<ul>
{%range $key, $val := .ingredients%}
   <li>
         {%$val.Name%} 
    </li>
{%end%}
</ul>
Products
<ul>
{%range $key, $val := .products%}
   <li>
         {%$val.Name%} 
         <br/>
         {%str2html $val.Description%}
         <br/>
    </li>
{%end%}
</ul>
Recipes
<ul>
{%range $key, $val := .recipes%}
   <li>
        {%$val.Name%} 
         <br/>
        {%str2html $val.Servings%}
        <br/>
        {%str2html $val.Steps%}
        <br/>
  </li>
{%end%}
</ul>

