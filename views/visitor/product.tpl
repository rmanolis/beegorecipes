{% template "base.tpl" . %}

{% define "content" %}

<div ng-controller="ProductCtrl">
<div ng-if="!product.IsChoosen">
<button ng-click="choose()">Choose</button>
</div>
<div ng-if="product.IsChoosen">
<button ng-click="reject()">Reject</button>
</div>


<div class="ui-widget">
Name: <span >{%.name%}</span>
<br/>

Description: <span>{%str2html .description%}</span>
<br/>

Price: <span >{%.price%}</span>
<br/>


Tags: <span >{%.tags%}</span>
<br/>

Ingredients: <span >{%.ingredients%}</span>
<br/>

</div>
</div>


{%end%}

{% template "visitor/basket.tpl" . %}

<div class="basket">
{% define "basket" %}
{%end%}
</div>

