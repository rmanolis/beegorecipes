{% template "base.tpl" . %}

{% define "content" %}


<ul>
{%range $key, $val := .products%}
   <li>
    <a href="/products/{%$val.Id.Hex%}">
      {%$val.Name%} 
    </a>
    </li>
{%end%}
</ul>


{%if gt .paginator.PageNums 1%}
<ul class="pagination pagination-sm">
    {%if .paginator.HasPrev%}
        <li><a href="{%.paginator.PageLinkFirst%}"></a></li>
        <li><a href="{%.paginator.PageLinkPrev%}">&lt;</a></li>
    {%else%}
        <li class="disabled"><a></a></li>
        <li class="disabled"><a>&lt;</a></li>
    {%end%}
    {%range $index, $page := .paginator.Pages%}
        <li{%if $.paginator.IsActive .%} class="active"{%end%}>
            <a href="{%$.paginator.PageLink $page%}">{%$page%}</a>
        </li>
    {%end%}
    {%if .paginator.HasNext%}
        <li><a href="{%.paginator.PageLinkNext%}">&gt;</a></li>
        <li><a href="{%.paginator.PageLinkLast%}"></a></li>
    {%else%}
        <li class="disabled"><a>&gt;</a></li>
        <li class="disabled"><a></a></li>
    {%end%}
</ul>
{%end%}


{% end %}
