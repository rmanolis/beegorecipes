{% template "base.tpl" . %}

{% define "content" %}
<div ng-controller="RecipeCtrl">
<div ng-if="!recipe.IsChoosen">
<button ng-click="choose()">Choose</button>
</div>
<div ng-if="recipe.IsChoosen">
<button ng-click="reject()">Reject</button>
</div>

<div class="ui-widget">
Name: <span >{%.name%}</span>
<br/>

Servings: <span>{%str2html .servings%}</span>
<br/>

Execution: <span >{%str2html .execution%}</span>
<br/>


Tags: <span >{%.tags%}</span>
<br/>

Ingredients: <span >{%.ingredients%}</span>
<br/>

</div>
</div>

{%end%}

{% template "visitor/basket.tpl" . %}

<div class="basket">
{% define "basket" %}
{%end%}
</div>

