
{% template "base.tpl" . %}

{% define "content" %}

<div ng-controller="SearchProductsCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>
Ingredients: <input id="ingredients" ng-model="singredients" size="50" >
<br/>

<button  ng-click="submit()">Submit</button>

</div>

<div class="ui-widget">
  <ul ng-repeat="product in products">
    <li class="product">
        <a href="{{'/products/'+product.Id}}" > <strong >{{product.Name}}</strong></a>
        <div ng-if="!product.IsChoosen">
          <button ng-click="choose(product.Id)">Choose</button>
        </div>
        <div ng-if="product.IsChoosen">
          <button ng-click="reject(product.Id)">Reject</button>
        </div>

    </li>
  </ul>
</div>

<div id="pagination"></div>
</div>

{% end %}

{% template "visitor/basket.tpl" . %}

<div class="basket">
{% define "basket" %}
{%end%}
</div>

