
{% template "base.tpl" . %}

{% define "content" %}

<div ng-controller="SearchRecipesCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>
Ingredients: <input id="ingredients" ng-model="singredients" size="50" >
<br/>

<button  ng-click="submit()">Submit</button>
</div>

<div class="ui-widget">
  <ul ng-repeat="recipe in recipes">
    <li class="recipe">
        <a href="{{'/recipes/'+recipe.Id}}" > <strong >{{recipe.Name}}</strong></a>
        <div ng-if="!recipe.IsChoosen">
          <button ng-click="choose(recipe.Id)">Choose</button>
        </div>
        <div ng-if="recipe.IsChoosen">
          <button ng-click="reject(recipe.Id)">Reject</button>
        </div>

    </li>
  </ul>
</div>

<div id="pagination"></div>
</div>

{% end %}

{% template "visitor/basket.tpl" . %}

<div class="basket">
{% define "basket" %}
{%end%}
</div>


